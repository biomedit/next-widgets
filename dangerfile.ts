import { schedule } from 'danger';
import {
  praiseDependencyReduction,
  notifyTechnicalCoordinators,
  checkBreakingChangeFooter,
  warnNoTests,
} from '@biomedit/danger';

// Warn if there are changes in the application code but not in tests
warnNoTests(['./**/*.ts*'], ['./**/*.test.*', './**/*.stories.*'], 'frontend');

// Praise the author
schedule(() => praiseDependencyReduction('./package.json'));

// Notify technical coordinators about UX/UI changes
notifyTechnicalCoordinators();

// Warn if breaking change footer is missing
checkBreakingChangeFooter();
