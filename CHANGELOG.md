# Changelog

All notable changes to this project will be documented in this file.

## [19.2.3](https://gitlab.com/biomedit/next-widgets/-/releases/19.2.3) - 2024-11-25

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/19.2.2...19.2.3)


### 🧱 Build system and dependencies

- Upgrade next.js to v15 ([b446635](https://gitlab.com/biomedit/next-widgets/commit/b44663523d06eb80a8df0a4a51774a72153460c0)), Close #116
- Update storybook to v8 ([dd43b05](https://gitlab.com/biomedit/next-widgets/commit/dd43b0596a74e72f3676f0ce86ed8454edef0d08)), Close #91
- **node:** Use Node LTS codenames ([e67ab6a](https://gitlab.com/biomedit/next-widgets/commit/e67ab6a0e5120d43b75f479ad47cc78f5df883d2))

## [19.2.2](https://gitlab.com/biomedit/next-widgets/-/releases/19.2.2) - 2024-11-04

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/19.2.1...19.2.2)


### 🐛 Bug Fixes

- **Nav:** Use the drawer component instead of a simple nav ([6183866](https://gitlab.com/biomedit/next-widgets/commit/61838660554a7705515b91ff24946dbb23540b93))

### 👷 CI

- Pin node JS version ([d41c83e](https://gitlab.com/biomedit/next-widgets/commit/d41c83e1f1c18b24cd9a8c64e736056b15e67cc2)), Closes #115

### 🧱 Build system and dependencies

- **eslint:** Remove warning on unknown react version ([3f2e96a](https://gitlab.com/biomedit/next-widgets/commit/3f2e96ae36953efc6cafe66b30b9d176a9104ca7))

## [19.2.1](https://gitlab.com/biomedit/next-widgets/-/releases/19.2.1) - 2024-10-22

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/19.2.0...19.2.1)


### ♻️  Refactoring

- Follow eslint-plugin-typescript-sort-keys rules ([621300c](https://gitlab.com/biomedit/next-widgets/commit/621300c31b03d03b736a1513cf585b38208346c8))

### 👷 CI

- Fix plugins in flat-format eslint.config.mjs ([a2582c9](https://gitlab.com/biomedit/next-widgets/commit/a2582c9c296611f74119fb19148c9e89a50acd4a)), Closes #114

### 🧱 Build system and dependencies

- **eslint:** Update `eslint` to v9 ([4dacd12](https://gitlab.com/biomedit/next-widgets/commit/4dacd123e8df7b53ef0ab064d852d63c453953d7))

## [19.2.0](https://gitlab.com/biomedit/next-widgets/-/releases/19.2.0) - 2024-10-17

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/19.1.0...19.2.0)


### ✨ Features

- **validators:** Allow + in names ([09022bc](https://gitlab.com/biomedit/next-widgets/commit/09022bc474f4fe5554ce21df9f3d16d271cd7d1e))

## [19.1.0](https://gitlab.com/biomedit/next-widgets/-/releases/19.1.0) - 2024-10-08

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/19.0.0...19.1.0)


### ✨ Features

- **mustBeOneOf:** Allow case-insensitive search ([ac5d49c](https://gitlab.com/biomedit/next-widgets/commit/ac5d49ccfee666e3fd67cdd7e4bd01ad1f0e00a5))

### 👷 CI

- Rename npm script test:lint to check:lint ([6f24903](https://gitlab.com/biomedit/next-widgets/commit/6f24903eaa6c79a3ec562a17cee56cd0fb73356e))
- Fix file formatting with prettier ([6d9ca9d](https://gitlab.com/biomedit/next-widgets/commit/6d9ca9d40fecccaa023d7489d1b151b89a023bfb))
- Use flat config file for eslint ([4f30fd6](https://gitlab.com/biomedit/next-widgets/commit/4f30fd6eefa7796f76457e85ab3332cf68feb901)), Closes #95
- Remove .eslintrc.js ([d35aa59](https://gitlab.com/biomedit/next-widgets/commit/d35aa5955b0158ffbd38de33158a2332a6f43379))

## [19.0.0](https://gitlab.com/biomedit/next-widgets/-/releases/19.0.0) - 2024-10-04

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.4.1...19.0.0)

### ⚠ BREAKING CHANGES

- the `DeleteDialog` component does not support the following properties anymore:
  - title
  - text

### ✨ Features

- **DeleteDialog:** Make component less flexible ([f6e955e](https://gitlab.com/biomedit/next-widgets/commit/f6e955ec99dfac140f63b2f06dea7ed08256ec93)), ⚠ BREAKING CHANGE: the `DeleteDialog` component does not support the following properties anymore:   - title   - text
- Add ConfirmDialog component ([435569d](https://gitlab.com/biomedit/next-widgets/commit/435569d4637bcf2b127459d4d5a21ee5f3a004e5))
- **EnhancedTable:** Specify columns width by percentage ([1d389fb](https://gitlab.com/biomedit/next-widgets/commit/1d389fb982498e58d3168aef28647ceebf3e5f92)), Closes #67

### 🧱 Build system and dependencies

- **.eslintrc:** Trigger errors for `no-deprecated` rule ([d08ccb6](https://gitlab.com/biomedit/next-widgets/commit/d08ccb6cab51d8c6754fa094fdb2324217b511ad)), Closes #105

## [18.4.1](https://gitlab.com/biomedit/next-widgets/-/releases/18.4.1) - 2024-09-10

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.4.0...18.4.1)


### 🐛 Bug Fixes

- **CollapsibleNavItem:** Add background color to accordion details ([a9482ff](https://gitlab.com/biomedit/next-widgets/commit/a9482ffec1d3bd506d90002838b93e97b808a39b))

## [18.4.0](https://gitlab.com/biomedit/next-widgets/-/releases/18.4.0) - 2024-09-10

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.3.0...18.4.0)


### ✨ Features

- **nav:** Add CollapsibleNavItem component ([d172aa5](https://gitlab.com/biomedit/next-widgets/commit/d172aa5c5aa83c3963eb08528ae70e245f5c8356))

### 👷 CI

- **danger:** Consider stories as tests ([da2ac67](https://gitlab.com/biomedit/next-widgets/commit/da2ac67afae2847404bcba68480b0729fe4b8fc9))

## [18.3.0](https://gitlab.com/biomedit/next-widgets/-/releases/18.3.0) - 2024-09-06

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.2.1...18.3.0)


### ♻️  Refactoring

- **validators:** Use node URL implementation to validate urls ([fbb0906](https://gitlab.com/biomedit/next-widgets/commit/fbb09066ddbd2c350b586064671630cc1dcb4c7d))

### ✅ Test

- **LabelledField:** Add storybook story for type url ([0fc7e10](https://gitlab.com/biomedit/next-widgets/commit/0fc7e1030ad5a1cc48ac7910c8827e961066ab31))
- **validators:** Do not render any component when testing validators ([25c78cf](https://gitlab.com/biomedit/next-widgets/commit/25c78cfe2077594fc44fd5b3637e6c6558106dbb))

### ✨ Features

- **LabelledField:** Use node URL implementation to cleanup input for type url ([d6c0f85](https://gitlab.com/biomedit/next-widgets/commit/d6c0f85b5b1a559207c7457ca65ac230c4208c0d))

### 🐛 Bug Fixes

- **validators:** Do not trigger validators on empty input ([a8624d9](https://gitlab.com/biomedit/next-widgets/commit/a8624d9f616d717816fec037523ab3d69f51fc7b)), Close #104

## [18.2.1](https://gitlab.com/biomedit/next-widgets/-/releases/18.2.1) - 2024-07-22

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.2.0...18.2.1)


### 🧱 Build system and dependencies

- **bumpversion:** Add dry-run mode ([218cccc](https://gitlab.com/biomedit/next-widgets/commit/218cccca1e170576c8d0e86ad350bcb02b9e3376)), Close #93

## [18.2.0](https://gitlab.com/biomedit/next-widgets/-/releases/18.2.0) - 2024-06-26

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.1.2...18.2.0)


### ✨ Features

- **reducers:** Add retrieve action ([2446b39](https://gitlab.com/biomedit/next-widgets/commit/2446b39642878d85a2a0fbccbb753165f578cf1a))
- **toolbar:** Improve table title positioning ([0c414d2](https://gitlab.com/biomedit/next-widgets/commit/0c414d27fe4a49b90b2c34cbbf5dff984c7f9e69)), Closes #101

## [18.1.2](https://gitlab.com/biomedit/next-widgets/-/releases/18.1.2) - 2024-06-10

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.1.1...18.1.2)


### 🧱 Build system and dependencies

- Update dependency @testing-library/react to v16 ([4288949](https://gitlab.com/biomedit/next-widgets/commit/4288949300633fb3a178444a94a29a991b585009))

## [18.1.1](https://gitlab.com/biomedit/next-widgets/-/releases/18.1.1) - 2024-05-17

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.1.0...18.1.1)


### ♻️  Refactoring

- Use Next.js `Link` instead of MUI one ([0feca8e](https://gitlab.com/biomedit/next-widgets/commit/0feca8ec0ad505780ea1ff5b08f76153a004a4e4))

## [18.1.0](https://gitlab.com/biomedit/next-widgets/-/releases/18.1.0) - 2024-05-15

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.0.5...18.1.0)


### ✨ Features

- **search:** Move search widget to the left ([94397b8](https://gitlab.com/biomedit/next-widgets/commit/94397b8bd7206775a77c38b1e13fae74e2bd4dbb)), Closes #94

## [18.0.5](https://gitlab.com/biomedit/next-widgets/-/releases/18.0.5) - 2024-04-29

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.0.4...18.0.5)


### 🐛 Bug Fixes

- `npm run build` because of types ([2285b26](https://gitlab.com/biomedit/next-widgets/commit/2285b264023e44438f17a75be100e1c89f65ef84))

## [18.0.4](https://gitlab.com/biomedit/next-widgets/-/releases/18.0.4) - 2024-04-16

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.0.3...18.0.4)


### 🐛 Bug Fixes

- **AutocompleteField.tsx:** `helperText` should not be passed to `Autocomplete` ([3aa2bf1](https://gitlab.com/biomedit/next-widgets/commit/3aa2bf14cc10abe59bae89f81f3f7655a3a08e6d))

### 🧱 Build system and dependencies

- **@testing-library/react:** Move to v15 ([e532ae2](https://gitlab.com/biomedit/next-widgets/commit/e532ae216a913020ed779f2754ae5706d681bf56)), Closes #1139
- **bumpversion:** Use git-cliff for calculating next version ([51d327a](https://gitlab.com/biomedit/next-widgets/commit/51d327a65497732d192861d74c71140819fc7823))

## [18.0.3](https://gitlab.com/biomedit/next-widgets/-/releases/18.0.3) - 2024-03-27

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.0.2...18.0.3)


### 🐛 Bug Fixes

- **bumpversion.sh:** Pattern for spotting features to make a new minor release ([376b80d](https://gitlab.com/biomedit/next-widgets/commit/376b80da80de74d6e51c4c7c2c86194a5a6812a2))

## [18.0.2](https://gitlab.com/biomedit/next-widgets/-/releases/18.0.2) - 2024-03-21

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.0.1...18.0.2)


### ✨ Features

- **AutocompleteField:** Show helperText if there is no error message ([4de9deb](https://gitlab.com/biomedit/next-widgets/commit/4de9debba60f163d0014c3040002c5a747e12cb1))

## [18.0.1](https://gitlab.com/biomedit/next-widgets/-/releases/18.0.1) - 2024-03-19

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/18.0.0...18.0.1)


### ✨ Features

- **LabelledField:** Show helperText if there is no error message ([16e25c1](https://gitlab.com/biomedit/next-widgets/commit/16e25c1e9f3fc41a1b2a65f38cf5f71687c7fecc))

## [18.0.0](https://gitlab.com/biomedit/next-widgets/-/releases/18.0.0) - 2024-02-22

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/17.0.10...18.0.0)

### ⚠ BREAKING CHANGES

- Remove the following:

- `function resJsonBody`
- `type MockServer`
- `function setupMockApi`
- `class RequestVerifier`

### ✅ Test

- Drop dependency on msw ([4792448](https://gitlab.com/biomedit/next-widgets/commit/4792448735ac6bda46be50be6a79187467c0f468)), ⚠ BREAKING CHANGE: Remove the following:  - `function resJsonBody` - `type MockServer` - `function setupMockApi` - `class RequestVerifier`

### ✨ Features

- **PageBase:** Support optionally adding breadcrumbs ([cf3e886](https://gitlab.com/biomedit/next-widgets/commit/cf3e886244cd759ff0394a440252453d7a039658))

### 🧱 Build system and dependencies

- Remove husky (pre-commit hooks) and commitizen ([b169903](https://gitlab.com/biomedit/next-widgets/commit/b1699037a19f82c07596b8e282dd0070800734e7))

## [17.0.10](https://gitlab.com/biomedit/next-widgets/-/releases/17.0.10) - 2024-01-17

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/17.0.9...17.0.10)


### ✨ Features

- **Tab:** Use title case instead of uppercase for labels ([970b10f](https://gitlab.com/biomedit/next-widgets/commit/970b10fadfb8e7697aa43eaf17bdca86811127a6))

### 👷 CI

- Change name of template to lint-commit.yml ([a95fcc4](https://gitlab.com/biomedit/next-widgets/commit/a95fcc41a3c77319a8c5aa25eb514bb6eb2e7af0))

## [17.0.8](https://gitlab.com/biomedit/next-widgets/-/releases/17.0.8) - 2023-12-21

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/17.0.8...17.0.8)


### ✅ Test

- Remove `undici` and fix `jsdom` environment ([46eeaaa](https://gitlab.com/biomedit/next-widgets/commit/46eeaaaf8e2796e147bc867374371fe1006a3b1b))

## [17.0.7](https://gitlab.com/biomedit/next-widgets/-/releases/17.0.7) - 2023-12-21

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/17.0.7...17.0.7)


### ✨ Features

- Add `addReduxDispatch` and replace deprecated `AnyAction` ([e936740](https://gitlab.com/biomedit/next-widgets/commit/e93674067600efd99f26b7da1886e07666e9646f))

## [17.0.5](https://gitlab.com/biomedit/next-widgets/-/releases/17.0.5) - 2023-11-16

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/17.0.5...17.0.5)


### ✨ Features

- **footer:** `variant` can be customized on top component (`Footer.tsx`) ([13d5edb](https://gitlab.com/biomedit/next-widgets/commit/13d5edb0822f60ca8fd797ba551c7fa2ffcd34e8))

## [17.0.4](https://gitlab.com/biomedit/next-widgets/-/releases/17.0.4) - 2023-11-15

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/17.0.4...17.0.4)


### ✨ Features

- **footer:** Add privacy link to footer ([3aab69b](https://gitlab.com/biomedit/next-widgets/commit/3aab69ba836bd944507bf05d4f253a8ad4088b8e)), Closes #90

## [17.0.1](https://gitlab.com/biomedit/next-widgets/-/releases/17.0.1) - 2023-09-11

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/17.0.0...17.0.1)


### 📝 Documentation

- **changelog:** Include all non-chore commits ([511c4f3](https://gitlab.com/biomedit/next-widgets/commit/511c4f32e315270a2bd5574dacb2ba6546a27030)), Close #86

### 🧱 Build system and dependencies

- Remove `emittery` dependency ([b946eac](https://gitlab.com/biomedit/next-widgets/commit/b946eacef73f33acce3df06896c6b1253e5c0ccb)), Closes #87
- Remove `serialize-error` dependency ([a15ea0e](https://gitlab.com/biomedit/next-widgets/commit/a15ea0e3a34fc3fe94fe04ae3ad370b6abfa5364)), Closes #84
- Remove `overrides` for `eslint-plugin-typescript-sort-keys` ([cb18068](https://gitlab.com/biomedit/next-widgets/commit/cb1806867998d73b8a827f486cef49c923042e9a))
- **git-cliff:** Include build commits when they have breaking changes ([4e3f4f7](https://gitlab.com/biomedit/next-widgets/commit/4e3f4f70943c69fbdc33f7ea2159627bebb0432c))

## [17.0.0](https://gitlab.com/biomedit/next-widgets/-/releases/17.0.0) - 2023-07-25

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/16.0.1...17.0.0)

### ⚠ BREAKING CHANGES

- `@mui/x-date-pickers` is no longer bundled into the library

### Features

- **validators:** Add mustBeOneOf ([8b9bd5e](https://gitlab.com/biomedit/next-widgets/commit/8b9bd5e93fb73af0d7deb524a43b47de6848f0c3))

### Build

- Move @mui/x-date-pickers to peerDependencies ([94315a7](https://gitlab.com/biomedit/next-widgets/commit/94315a7aabe72101b36910e9925666b2b74eae55)), ⚠ BREAKING CHANGE: `@mui/x-date-pickers` is no longer bundled into the library

## [16.0.1](https://gitlab.com/biomedit/next-widgets/-/releases/16.0.1) - 2023-07-19

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/16.0.0...16.0.1)


### Bug Fixes

- **PageBase:** Reintroduce page title ([13fdc6d](https://gitlab.com/biomedit/next-widgets/commit/13fdc6d11160f9506644630c24321323db79ebb6))

## [16.0.0](https://gitlab.com/biomedit/next-widgets/-/releases/16.0.0) - 2023-07-18

[See all changes since the last release](https://gitlab.com/biomedit/next-widgets/compare/15.6.0...16.0.0)

### ⚠ BREAKING CHANGES

- - Change type of property `title` of `PageBaseProps`.
- Remove `StyleMap`.

### Features

- **PageBase:** Remove breadcrumb and title ([01ac83c](https://gitlab.com/biomedit/next-widgets/commit/01ac83c02fbcd23c9e50cbbe9eb9768b656f7fbc)), ⚠ BREAKING CHANGE: - Change type of property `title` of `PageBaseProps`. - Remove `StyleMap`.

## [15.6.0](https://gitlab.com/biomedit/next-widgets/compare/15.5.2...15.6.0) (2023-06-24)


### Features

* **TableToolbar:** add `isExporting` which should enable/disable the export button ([31c34ce](https://gitlab.com/biomedit/next-widgets/commit/31c34ce418c85ad1d0b70fcc2482722c70dffbe4))

### [15.5.2](https://gitlab.com/biomedit/next-widgets/compare/15.5.1...15.5.2) (2023-06-19)

### [15.5.1](https://gitlab.com/biomedit/next-widgets/compare/15.5.0...15.5.1) (2023-06-09)

## [15.5.0](https://gitlab.com/biomedit/next-widgets/compare/15.4.3...15.5.0) (2023-05-10)


### Features

* **CheckboxField:** needs to support ReactNode as label not string ([a996575](https://gitlab.com/biomedit/next-widgets/commit/a9965751c58ad691376ccf7831f8b4eeb31b02a8)), closes [#85](https://gitlab.com/biomedit/next-widgets/issues/85)
* **ListPage:** `labelWidth` should be parameterizable ([10cadcf](https://gitlab.com/biomedit/next-widgets/commit/10cadcf1386587a6e97eca7e09c0a4adf57254b4))

### [15.4.3](https://gitlab.com/biomedit/next-widgets/compare/15.4.2...15.4.3) (2023-04-16)


### Bug Fixes

* no `Response` thrown, only `Error` ([e3c062e](https://gitlab.com/biomedit/next-widgets/commit/e3c062e2ea59896fa07dd54b311e55e1d2c1565d))

### [15.4.2](https://gitlab.com/biomedit/next-widgets/compare/15.4.1...15.4.2) (2023-04-01)


### Bug Fixes

* **AutocompleteField:** because of `Tooltip` widget does no longer obey width constraints ([050e986](https://gitlab.com/biomedit/next-widgets/commit/050e986b4f19081e2b571e845f1ad2756104c915))

### [15.4.1](https://gitlab.com/biomedit/next-widgets/compare/15.4.0...15.4.1) (2023-03-29)


### Bug Fixes

* **LabelledField:** empty string is NOT a valid date ([2a3c7d4](https://gitlab.com/biomedit/next-widgets/commit/2a3c7d4e3f9e74d5938a33b83583dbe4400b40ca))

## [15.4.0](https://gitlab.com/biomedit/next-widgets/compare/15.3.0...15.4.0) (2023-03-28)


### Features

* **AutocompleteField:** add support for `title` (kind of tooltip) ([b20475d](https://gitlab.com/biomedit/next-widgets/commit/b20475d4db295361f0fdf07a3752e300eb65a909))
* **SelectField:** add support for default `helperText` ([a60eff4](https://gitlab.com/biomedit/next-widgets/commit/a60eff48e6d5a1ce822b74ec4a22ca5ba594c82f))

## [15.3.0](https://gitlab.com/biomedit/next-widgets/compare/15.2.0...15.3.0) (2023-03-20)


### Features

* extract `ToastBarSnackBar` ([419e53f](https://gitlab.com/biomedit/next-widgets/commit/419e53f3d316cbf2e9ca8326d4cbeb651a244ea5))

## [15.2.0](https://gitlab.com/biomedit/next-widgets/compare/15.1.0...15.2.0) (2023-03-14)


### Features

* return a nice `DatePicker` if type is `date` ([33d8054](https://gitlab.com/biomedit/next-widgets/commit/33d80540a5ddf40a9ae782d5b5d521d44f44ac82)), closes [#80](https://gitlab.com/biomedit/next-widgets/issues/80)
* **table toolbar:** add optional download button to table toolbar ([b7acff8](https://gitlab.com/biomedit/next-widgets/commit/b7acff8275e70395ee533c49b0b3a5853aec5988)), closes [#81](https://gitlab.com/biomedit/next-widgets/issues/81)


### Bug Fixes

* make `SelectField` smaller ([48a2658](https://gitlab.com/biomedit/next-widgets/commit/48a265813264b627a2346b65613cdf3daba72c20))

## [15.1.0](https://gitlab.com/biomedit/next-widgets/compare/15.0.0...15.1.0) (2023-03-09)


### Features

* add new widgets `CopyToClipboardTextField` and `CopyToClipboardPasswordField` ([5856952](https://gitlab.com/biomedit/next-widgets/commit/585695282a3e8f5e2cc08c4fc39cd0f7f846926b))

## [15.0.0](https://gitlab.com/biomedit/next-widgets/compare/14.0.1...15.0.0) (2023-03-01)


### ⚠ BREAKING CHANGES

* **utils:** - `isChristmas` has been removed
- `isNewYear` has been removed
- `isNationalDay` has been removed
- `isHalloween` has been removed
* **Footer:** - `Footer` now requires the `appName` property.
- `Version` now requires the `appName` property.

### Features

* **Footer:** add appName property ([ede446a](https://gitlab.com/biomedit/next-widgets/commit/ede446a7428e46435080a58b4da24000d99be5bb)), closes [#78](https://gitlab.com/biomedit/next-widgets/issues/78)
* **utils:** add getPageTitle ([99ebd69](https://gitlab.com/biomedit/next-widgets/commit/99ebd6906ee162bd019e1099d82c45186fd83ede))
* **utils:** remove trivial seasonal checks ([ebb80d0](https://gitlab.com/biomedit/next-widgets/commit/ebb80d0dc348973b9deaa2a01fbd14b951192345))

### [14.0.1](https://gitlab.com/biomedit/next-widgets/compare/14.0.0...14.0.1) (2023-02-20)

## [14.0.0](https://gitlab.com/biomedit/next-widgets/compare/13.1.2...14.0.0) (2023-02-14)


### ⚠ BREAKING CHANGES

* - The following packages are no longer bundled into the library:

  - `@tanstack/react-table`
  - `@testing-library/react`
  - `@types/marked`
  - `msw`
  - `react-i18next`

- `next` _v12.0.0_ is no longer supported as a `peerDependency`

### build

* optimize bundle size ([a5c4212](https://gitlab.com/biomedit/next-widgets/commit/a5c4212dfeed7583333a0c5a96a0aa501147b38b))

### [13.1.2](https://gitlab.com/biomedit/next-widgets/compare/13.1.1...13.1.2) (2023-02-06)

### [13.1.1](https://gitlab.com/biomedit/next-widgets/compare/13.1.0...13.1.1) (2023-01-30)

## [13.1.0](https://gitlab.com/biomedit/next-widgets/compare/13.0.0...13.1.0) (2023-01-25)


### Features

* **validators:** add validator for https urls ([97dda0b](https://gitlab.com/biomedit/next-widgets/commit/97dda0b58fe0e174bab60c7dfe845b5e4a521497)), closes [#72](https://gitlab.com/biomedit/next-widgets/issues/72)

## [13.0.0](https://gitlab.com/biomedit/next-widgets/compare/12.1.1...13.0.0) (2023-01-24)


### ⚠ BREAKING CHANGES

* **table/sortTypes:** `localeSortingFn` has been removed and replaced by a
hook.

### Bug Fixes

* **table/sortTypes:** change localeSortingFn into a hook ([3a25e5f](https://gitlab.com/biomedit/next-widgets/commit/3a25e5f42d3c093a350eddf2b09a73ad01419e01))

### [12.1.1](https://gitlab.com/biomedit/next-widgets/compare/12.1.0...12.1.1) (2023-01-23)

## [12.1.0](https://gitlab.com/biomedit/next-widgets/compare/12.0.1...12.1.0) (2023-01-19)


### Features

* **EnhancedTable:** add tooltip for global filter ([3750cad](https://gitlab.com/biomedit/next-widgets/commit/3750cade8e551d5d7e2177e60e1678fe7a6ffd7f)), closes [#69](https://gitlab.com/biomedit/next-widgets/issues/69)
* **Timeline:** change feed icon warning color ([e8186b9](https://gitlab.com/biomedit/next-widgets/commit/e8186b9580e9faadf6c465c548f435ec58850432)), closes [#68](https://gitlab.com/biomedit/next-widgets/issues/68)
* **util/dates:** add human readable date formatting to formatDate function ([ece6b67](https://gitlab.com/biomedit/next-widgets/commit/ece6b6716c9959ab3326af72c6b310d24f66baaf)), closes [#70](https://gitlab.com/biomedit/next-widgets/issues/70)

### [12.0.1](https://gitlab.com/biomedit/next-widgets/compare/12.0.0...12.0.1) (2023-01-09)

## [12.0.0](https://gitlab.com/biomedit/next-widgets/compare/11.5.1...12.0.0) (2022-12-19)


### ⚠ BREAKING CHANGES

* **react-table:** `EnhancedTable`'s `globalFilter` property is now called `globalFilterFn`.
`EnhancedTable`'s `columns` property is now of type `ColumnDef[]` instead of `Column[]`.

### build

* **react-table:** migrate to v8 ([db1bc25](https://gitlab.com/biomedit/next-widgets/commit/db1bc2564e29cf6dfd5e1d6ab798b2968fd2de2e)), closes [#59](https://gitlab.com/biomedit/next-widgets/issues/59)

### [11.5.1](https://gitlab.com/biomedit/next-widgets/compare/11.5.0...11.5.1) (2022-12-19)

## [11.5.0](https://gitlab.com/biomedit/next-widgets/compare/11.4.0...11.5.0) (2022-12-12)


### Features

* **validators:** add support for FTPS to URL validator ([fdaab49](https://gitlab.com/biomedit/next-widgets/commit/fdaab498e43c66d420178e8516959a0ffae50b48))

## [11.4.0](https://gitlab.com/biomedit/next-widgets/compare/11.3.5...11.4.0) (2022-11-30)


### Features

* **Header:** add `priority` as optional argument ([5cf0a88](https://gitlab.com/biomedit/next-widgets/commit/5cf0a8815401e6c661d5a9f840f2d9d4babc7671))


### Bug Fixes

* **ToastBar:** throws an exception on invocation ([64a8df9](https://gitlab.com/biomedit/next-widgets/commit/64a8df94de8466d75ea43db9c450e5cc77604e3f))

### [11.3.5](https://gitlab.com/biomedit/next-widgets/compare/11.3.4...11.3.5) (2022-11-21)

### [11.3.4](https://gitlab.com/biomedit/next-widgets/compare/11.3.3...11.3.4) (2022-11-16)


### Bug Fixes

* **next/link:** `passHref` does no longer work in `NavItem` and generates two `<a>` ([1765cea](https://gitlab.com/biomedit/next-widgets/commit/1765cea3d19b4c7ffd1abacc1008c33d8d021d2e))

### [11.3.3](https://gitlab.com/biomedit/next-widgets/compare/11.3.2...11.3.3) (2022-11-14)

### [11.3.2](https://gitlab.com/biomedit/next-widgets/compare/11.3.1...11.3.2) (2022-11-08)

### [11.3.1](https://gitlab.com/biomedit/next-widgets/compare/11.3.0...11.3.1) (2022-11-08)


### Bug Fixes

* **UserMenu:** use theme style ([d01a21e](https://gitlab.com/biomedit/next-widgets/commit/d01a21e29cdc16ff0112ec95166dbff6e4a30e9a))

## [11.3.0](https://gitlab.com/biomedit/next-widgets/compare/11.2.0...11.3.0) (2022-11-04)


### Features

* **theme:** add BioMedIT theme ([8afbf4a](https://gitlab.com/biomedit/next-widgets/commit/8afbf4a272d58ce3ab4438c95b62c978dc4744c2))


### Bug Fixes

* **EnhancedTable:** align column headers on small screen sizes ([cdb014f](https://gitlab.com/biomedit/next-widgets/commit/cdb014f7ffe6757349111d8144faba361dbcb60a)), closes [#53](https://gitlab.com/biomedit/next-widgets/issues/53)

## [11.2.0](https://gitlab.com/biomedit/next-widgets/compare/11.1.0...11.2.0) (2022-11-01)


### Features

* **SelectField:** add property to populate field when only one option is available ([4fce9ab](https://gitlab.com/biomedit/next-widgets/commit/4fce9ab345195b9db47d3dba1f181f9bc5f9b13d)), closes [#63](https://gitlab.com/biomedit/next-widgets/issues/63)

## [11.1.0](https://gitlab.com/biomedit/next-widgets/compare/11.0.2...11.1.0) (2022-10-31)


### Features

* **PageBase:** add `title` attribute to `head` of pages ([6ad2257](https://gitlab.com/biomedit/next-widgets/commit/6ad22573fbad8d05a4fec6b3bdc749ee6692c731)), closes [portal#700](https://gitlab.com/biomedit/portal/issues/700)


### Bug Fixes

* **AutoSelect:** display all the entries with a scrollbar ([47e4158](https://gitlab.com/biomedit/next-widgets/commit/47e4158a58601a4ea471d749ef0deb0b8154b500)), closes [#62](https://gitlab.com/biomedit/next-widgets/issues/62)

### [11.0.2](https://gitlab.com/biomedit/next-widgets/compare/11.0.1...11.0.2) (2022-10-26)

### [11.0.1](https://gitlab.com/biomedit/next-widgets/compare/11.0.0...11.0.1) (2022-10-26)


### Bug Fixes

* **AutoSelect:** expose 'onInputChange' ([13e989f](https://gitlab.com/biomedit/next-widgets/commit/13e989ff8dfdaf6f699fe670a941a05b8beb9a6d))

## [11.0.0](https://gitlab.com/biomedit/next-widgets/compare/10.0.0...11.0.0) (2022-10-26)


### ⚠ BREAKING CHANGES

* **npm:** `textFieldProps` and `onSelect` have been removed from
`AutoSelect` props, and we expect `choices` to extend `AutocompleteOption`.

### Features

* **components:** add `SkipLink` component ([2633645](https://gitlab.com/biomedit/next-widgets/commit/2633645374e8031181dfe982c8779f2adc4609fc))


### build

* **npm:** update all npm dependencies (2022-10-24) ([836b984](https://gitlab.com/biomedit/next-widgets/commit/836b984d367c1371378704f6aac3e3d9de059554)), closes [#5](https://gitlab.com/biomedit/next-widgets/issues/5)

## [10.0.0](https://gitlab.com/biomedit/next-widgets/compare/9.9.1...10.0.0) (2022-10-20)


### ⚠ BREAKING CHANGES

* **ColoredStatus:** `status.color` allowed values have changed.

### Features

* change colors to theme's primary ([9f65814](https://gitlab.com/biomedit/next-widgets/commit/9f65814278abb66dc3471c79726617657aa91b4b))
* **CheckboxArrayField:** use Mui Checkbox instead of default one ([e81f940](https://gitlab.com/biomedit/next-widgets/commit/e81f94032a2d8069c1a918f663a521e1e383ce92))
* **ColoredStatus:** turn ColoredStatus into a Chip ([e4c2e7c](https://gitlab.com/biomedit/next-widgets/commit/e4c2e7c201514c2ddf526759e6ff9f459abe2e21))
* **EnhancedTable:** use colored buttons ([ddbaecc](https://gitlab.com/biomedit/next-widgets/commit/ddbaecc190c0aa449c9f5d34211531026bfa5fd3))
* **Header:** allow header to be a link ([dc84baa](https://gitlab.com/biomedit/next-widgets/commit/dc84baabd0d15c9c10e8955657c8f09524ae28d1))
* remove shadows ([222e6d0](https://gitlab.com/biomedit/next-widgets/commit/222e6d07acc5af79e97eae914a29f2795e2c27a9))

### [9.9.1](https://gitlab.com/biomedit/next-widgets/compare/9.9.0...9.9.1) (2022-10-10)

## [9.9.0](https://gitlab.com/biomedit/next-widgets/compare/9.8.0...9.9.0) (2022-10-06)


### Features

* **Tab:** expose `TabItem` ID specification ([1c3a084](https://gitlab.com/biomedit/next-widgets/commit/1c3a0847ccf973cfa1e2b4f865b6621d94d25c81))


### Bug Fixes

* **EnhancedTable:** use IdType instead of IdRequired ([2a0707b](https://gitlab.com/biomedit/next-widgets/commit/2a0707bb99e3349b3b4e727b3177373da2fd6cc7))

## [9.8.0](https://gitlab.com/biomedit/next-widgets/compare/9.7.2...9.8.0) (2022-09-28)


### Features

* **ListPage:** make expanded `Accordion` configurable ([1a14a88](https://gitlab.com/biomedit/next-widgets/commit/1a14a889c321880fa3b095ec65d34020ef91e493))


### Bug Fixes

* **typescript:** compilation errors due to upgrade ([e1bcb2b](https://gitlab.com/biomedit/next-widgets/commit/e1bcb2b7f3f775cece0cf2795b22754c01fae0c2)), closes [#60](https://gitlab.com/biomedit/next-widgets/issues/60)

### [9.7.2](https://gitlab.com/biomedit/next-widgets/compare/9.7.1...9.7.2) (2022-09-19)

### [9.7.1](https://gitlab.com/biomedit/next-widgets/compare/9.7.0...9.7.1) (2022-07-28)

## [9.7.0](https://gitlab.com/biomedit/next-widgets/compare/9.6.0...9.7.0) (2022-07-07)


### Features

* **Buttons:** force FabButtons to a 1/1 ratio ([779ad8b](https://gitlab.com/biomedit/next-widgets/commit/779ad8be62c547692a299921a9abb6a5c6a4f998)), closes [#46](https://gitlab.com/biomedit/next-widgets/issues/46)

## [9.6.0](https://gitlab.com/biomedit/next-widgets/compare/9.5.0...9.6.0) (2022-06-10)


### Features

* **EnhancedTable:** allow to define additional action buttons ([7cc73eb](https://gitlab.com/biomedit/next-widgets/commit/7cc73ebae270e0f7bd5b3bda56ec2f48bdd67188)), closes [#49](https://gitlab.com/biomedit/next-widgets/issues/49)
* **EnhancedTable:** make the hover over text in the project header field names more explanatory ([bd4181f](https://gitlab.com/biomedit/next-widgets/commit/bd4181fece2eac50f649114b056c9ec5f78ae1df)), closes [#50](https://gitlab.com/biomedit/next-widgets/issues/50)

## [9.5.0](https://gitlab.com/biomedit/next-widgets/compare/9.4.0...9.5.0) (2022-05-25)


### Features

* **TabPane:** add panelBoxProps ([c385b25](https://gitlab.com/biomedit/next-widgets/commit/c385b25e582dc9f76281c4ca6e40adf20c6e8f70)), closes [#41](https://gitlab.com/biomedit/next-widgets/issues/41)

## [9.4.0](https://gitlab.com/biomedit/next-widgets/compare/9.3.0...9.4.0) (2022-05-19)


### Features

* **validators:** add pgpFingerprint and mustBe validators ([c059f0e](https://gitlab.com/biomedit/next-widgets/commit/c059f0e5daada9d485ffec376fc46f04fbec2f81))

## [9.3.0](https://gitlab.com/biomedit/next-widgets/compare/9.2.0...9.3.0) (2022-05-10)


### Features

* **ColoredStatus:** `component` as possible `ColoredStatus` argument ([94b40b5](https://gitlab.com/biomedit/next-widgets/commit/94b40b59155fd8ff2a6aa6828ffc79badef7deac))


### Bug Fixes

* **validator:** relax constraints on group name ([b16a31f](https://gitlab.com/biomedit/next-widgets/commit/b16a31ff4a1d1cbdb6a94620523bbc6d3535bec1)), closes [#44](https://gitlab.com/biomedit/next-widgets/issues/44)

## [9.2.0](https://gitlab.com/biomedit/next-widgets/compare/9.1.0...9.2.0) (2022-05-06)


### Features

* **MultilineField:** add MultilineField component ([cb1826d](https://gitlab.com/biomedit/next-widgets/commit/cb1826d2ff30487108dc5c92c7ba59573da2c647))

## [9.1.0](https://gitlab.com/biomedit/next-widgets/compare/9.0.0...9.1.0) (2022-05-06)


### Features

* add `lightgrey` as possible `StatusColor` option ([235a279](https://gitlab.com/biomedit/next-widgets/commit/235a279935b342d71f9ce4cf59ac96df665a3ad8))


### Bug Fixes

* **EnhancedTable:** force action buttons on the same line ([7305061](https://gitlab.com/biomedit/next-widgets/commit/730506123e5b4cbf40668ff6ec9dad9ebb9cfa13))

## [9.0.0](https://gitlab.com/biomedit/next-widgets/compare/8.2.0...9.0.0) (2022-04-26)


### ⚠ BREAKING CHANGES

* **DeleteDialog:** Below are listed the components/functions that present breaking changes.

**DeleteDialog**

- The `deleteItemName` parameter has been renamed to `confirmationText`.
- The `confirmationText` parameter has been renamed to `confirmationHelper`.

**ListBase**

- The `deleteItemName` parameter has been renamed to `deleteConfirmationText`.

**useList & useDeleteDialog**

- The `getItemName` parameter has been renamed to `getDeleteConfirmationText`.
- The `deleteItemName` return value has been renamed to `deleteConfirmationText`.

**ListPage & EnhancedTable**

- The `getItemName` parameter has been renamed to `getDeleteConfirmationText`.

* **DeleteDialog:** change naming of confirmation text related parameters ([22df495](https://gitlab.com/biomedit/next-widgets/commit/22df495e8809d71933f94c316feaa52544171e34))

## [8.2.0](https://gitlab.com/biomedit/next-widgets/compare/8.1.0...8.2.0) (2022-04-22)


### Features

* **buttons:** change delete button color to red ([cc81e82](https://gitlab.com/biomedit/next-widgets/commit/cc81e82f7044d7da6fd6a914291fc405b62eb6ba)), closes [#40](https://gitlab.com/biomedit/next-widgets/issues/40)
* **DeleteDialog:** ask for a safer confirmation text ([911c841](https://gitlab.com/biomedit/next-widgets/commit/911c841c5d2ce5d444de26aeff2702d42f706f39))
* **ListHooks:** expand ListModel typing to accept ReactElements as caption ([16cd3b2](https://gitlab.com/biomedit/next-widgets/commit/16cd3b2b6077c2d035db5957d5b8b361ecbd973a))
* **Stepper:** use theme instead of hardcoded colors ([11268af](https://gitlab.com/biomedit/next-widgets/commit/11268af50607a140492ae858cc79298c8e20d386))

## [8.1.0](https://gitlab.com/biomedit/next-widgets/compare/8.0.3...8.1.0) (2022-03-30)


### Features

* in case of handled error, serialize the error if it's a Response ([38e8e83](https://gitlab.com/biomedit/next-widgets/commit/38e8e838bac64f6667e03dca22822a13dc30343f))

### [8.0.3](https://gitlab.com/biomedit/next-widgets/compare/8.0.2...8.0.3) (2022-03-16)


### Bug Fixes

* **EnhancedTable:** fix canEdit and canDelete check for showActions display column ([ec95768](https://gitlab.com/biomedit/next-widgets/commit/ec95768dc3315144ac87eb5108a6433f8f6380f5)), closes [#32](https://gitlab.com/biomedit/next-widgets/issues/32)

### [8.0.2](https://gitlab.com/biomedit/next-widgets/compare/8.0.1...8.0.2) (2022-03-11)


### Bug Fixes

* replace 'snarkdown' with 'marked' as Markdown renderer ([b168408](https://gitlab.com/biomedit/next-widgets/commit/b168408034d3113edd76a2265662cbea75e05244)), closes [#10](https://gitlab.com/biomedit/next-widgets/issues/10)

### [8.0.1](https://gitlab.com/biomedit/next-widgets/compare/8.0.0...8.0.1) (2022-03-08)


### Bug Fixes

* when retrieving, we are submitting, and NOT fetching ([86dcc03](https://gitlab.com/biomedit/next-widgets/commit/86dcc0334acd4cd1a2dc263fac0c3b0585ef1a8a))

## [8.0.0](https://gitlab.com/biomedit/next-widgets/compare/7.1.0...8.0.0) (2022-03-07)


### ⚠ BREAKING CHANGES

* Import directly from `@mui/material` instead and be
aware of tree-shaking (see https://mui.com/guides/minimizing-bundle-size/)
in development mode.

### Features

* add `retrieve` action type ([265d2e9](https://gitlab.com/biomedit/next-widgets/commit/265d2e9b6216f28f1e8f727c24372aa2582fb9e6))
* remove pure or simple Material UI widgets ([4ed51c8](https://gitlab.com/biomedit/next-widgets/commit/4ed51c81c6821670eed4632d7e09de2ee6149ebf)), closes [#36](https://gitlab.com/biomedit/next-widgets/issues/36)

## [7.1.0](https://gitlab.com/biomedit/next-widgets/compare/7.0.0...7.1.0) (2022-02-25)


### Features

* **form/validations:** make noValidate property optional ([8c56854](https://gitlab.com/biomedit/next-widgets/commit/8c56854820d96d0a423354a95d5f2b461b36e9f5))

## [7.0.0](https://gitlab.com/biomedit/next-widgets/compare/6.11.1...7.0.0) (2022-02-17)


### ⚠ BREAKING CHANGES

* **Checkbox:** `handleChange` of `CheckboxArrayFieldProps` has been renamed to `onChange`.

### Features

* **Dialog:** do not limit confirm/cancel label to enum values ([0fe4619](https://gitlab.com/biomedit/next-widgets/commit/0fe4619506dfb4ea2682c244816050b58b8d5f72))
* **FormDialog:** `title` could be a React node and not only a string ([ffbd6a0](https://gitlab.com/biomedit/next-widgets/commit/ffbd6a0d5062143a89f8482b56c9cdcdd1753141))


### Bug Fixes

* **Form/validations:** disable HTML 5 validations on Form component ([1385048](https://gitlab.com/biomedit/next-widgets/commit/138504830308290cb88789963c8b2e2a37005ee7)), closes [#34](https://gitlab.com/biomedit/next-widgets/issues/34)


* **Checkbox:** remove hacks and workarounds ([e9f5ce4](https://gitlab.com/biomedit/next-widgets/commit/e9f5ce4629ef66154e08e1e9d761f2855a93f190)), closes [#33](https://gitlab.com/biomedit/next-widgets/issues/33)

### [6.11.1](https://gitlab.com/biomedit/next-widgets/compare/6.11.0...6.11.1) (2022-02-02)


### Bug Fixes

* **EnhancedArrayFieldProps:** use `key` instead of `id` for TKeyName ([d5f6b95](https://gitlab.com/biomedit/next-widgets/commit/d5f6b95dea3041505a3e9dee162c55af01435bef))

## [6.11.0](https://gitlab.com/biomedit/next-widgets/compare/6.10.0...6.11.0) (2022-01-19)


### Features

* **AutocompleteField:** add possibility to externally reset to empty choice ([7f88dcf](https://gitlab.com/biomedit/next-widgets/commit/7f88dcf57e09d3c2bdfd9c8bc98a7bb3254fe80c))

## [6.10.0](https://gitlab.com/biomedit/next-widgets/compare/6.9.0...6.10.0) (2021-12-23)


### Features

* **commitizen:** add commitizen to the project ([23869c7](https://gitlab.com/biomedit/next-widgets/commit/23869c719725ebffa6c4031f1d875296bd88ab08))
* **commitlint:** add commitlint pre-commit hook ([2115258](https://gitlab.com/biomedit/next-widgets/commit/21152587e389c533c8a659797b818942a4820f77))


### Bug Fixes

* **husky:** fix husky to restore pre commit hooks ([f8cfe5c](https://gitlab.com/biomedit/next-widgets/commit/f8cfe5c4bbf26ba07b934fe55e9ea16ce321f4b9))

## [6.9.0](https://gitlab.com/biomedit/next-widgets/compare/6.8.1...6.9.0) (2021-12-18)


### Features

* **formatDate:** accept `null` as input value as well ([16c0d33](https://gitlab.com/biomedit/next-widgets/commit/16c0d334b887f68ffb8cf4124a1e9c5cab8378b3))

### [6.8.1](https://gitlab.com/biomedit/next-widgets/compare/6.8.0...6.8.1) (2021-12-17)

## [6.8.0](https://gitlab.com/biomedit/next-widgets/compare/6.7.0...6.8.0) (2021-12-16)


### Features

* **testUtils:** add method `expectAllToBeInTheDocument` to allow passing in an array of elements ([5558e55](https://gitlab.com/biomedit/next-widgets/commit/5558e5596f91a176984eb76a31d498efa7d937d4))

## [6.7.0](https://gitlab.com/biomedit/next-widgets/compare/6.6.1...6.7.0) (2021-12-10)


### Features

* **Buttons:** add buttons for marking email (un)read ([6cb6563](https://gitlab.com/biomedit/next-widgets/commit/6cb6563e98d0778cb748ba8f99b3537e0196848b))

### [6.6.1](https://gitlab.com/biomedit/next-widgets/compare/6.6.0...6.6.1) (2021-12-08)


### Bug Fixes

* **reducerBase:** convert id to string for del and update reducers ([8737e60](https://gitlab.com/biomedit/next-widgets/commit/8737e604ae08ed206ae5d66986006712bf4264ae))

## [6.6.0](https://gitlab.com/biomedit/next-widgets/compare/6.5.1...6.6.0) (2021-11-30)


### Features

* **EnhancedTable:** make column width customizable ([6670b03](https://gitlab.com/biomedit/next-widgets/commit/6670b0391eae2227586c2b05ea69edb6b8efe2b1))

### [6.5.1](https://gitlab.com/biomedit/next-widgets/compare/6.5.0...6.5.1) (2021-11-23)

## [6.5.0](https://gitlab.com/biomedit/next-widgets/compare/6.4.0...6.5.0) (2021-11-20)


### Features

* **utils/seasonal:** add new seasonal functions ([4346aa6](https://gitlab.com/biomedit/next-widgets/commit/4346aa60718ccd8f77b645ed1f84c1e436a903c6))

## [6.4.0](https://gitlab.com/biomedit/next-widgets/compare/6.3.0...6.4.0) (2021-11-17)


### Features

* **AutocompleteField:** Autocomplete `TextField` size should be parameterizable with default to 'small' ([9839d21](https://gitlab.com/biomedit/next-widgets/commit/9839d21406b79e3e7dd1749f33c43403d025c179))


### Bug Fixes

* **AutocompleteField:** clear selected choice(s) if they are not present in choices (anymore) ([884b95f](https://gitlab.com/biomedit/next-widgets/commit/884b95f3903ed4a2139086f7206fd04fc538b411))

## [6.3.0](https://gitlab.com/biomedit/next-widgets/compare/6.2.0...6.3.0) (2021-11-12)


### Features

* use correlationId from header of a response in error handling ([3f23fc3](https://gitlab.com/biomedit/next-widgets/commit/3f23fc39c8a496cb4cdb3129b4a3dda7ba65d991))

## [6.2.0](https://gitlab.com/biomedit/next-widgets/compare/6.1.0...6.2.0) (2021-11-10)


### Features

* **SelectField:** make `SelectField` more customizable ([386e2a7](https://gitlab.com/biomedit/next-widgets/commit/386e2a7ec2ecc55fb4a60251f8c39cae7e34539d))

## [6.1.0](https://gitlab.com/biomedit/next-widgets/compare/6.0.2...6.1.0) (2021-11-02)


### Features

* **AutocompleteField:** allow passing in an `onChange` callback ([76de326](https://gitlab.com/biomedit/next-widgets/commit/76de326d5fe0a467a0f13bf2168ea84a2355b895))

### [6.0.2](https://gitlab.com/biomedit/next-widgets/compare/6.0.1...6.0.2) (2021-10-28)

### [6.0.1](https://gitlab.com/biomedit/next-widgets/compare/6.0.0...6.0.1) (2021-10-21)


### Bug Fixes

* **HiddenField:** allow falsy values like `false` and `0` to be used as `value` ([0a9770d](https://gitlab.com/biomedit/next-widgets/commit/0a9770d59ab1cc1fe9914456eb890e53f31e2acf))
* **HiddenField:** make sure form state is always updated when `defaultValue` changes ([1e09d1f](https://gitlab.com/biomedit/next-widgets/commit/1e09d1ff3599cb16fa9cab32076cdf39d0a5ab44)), closes [portal#458](https://gitlab.com/biomedit/portal/issues/458)
* **sagas:** fix type error ([24e51e8](https://gitlab.com/biomedit/next-widgets/commit/24e51e80b5b2d218b37f755af199581df3637d30))

## [6.0.0](https://gitlab.com/biomedit/next-widgets/compare/5.0.0...6.0.0) (2021-10-13)


### ⚠ BREAKING CHANGES

* **npm:** It is only possible to import `from '@biomedit/next-widgets'`.
Importing from a subpath, like `from '@biomedit/next-widgets/esm/lib/widgets/Tooltip'`
is no longer possible.

Not having an export map allows importing from any file, even if it is not exported in
the main `index.ts` file.
This means users could accidentally import from private files we do not choose to
export.
Should we change something in a non-exported file and a user accidentally imported it,
it could result in a breaking change for them even if we do not declare a release to
include one.
An additional advantage is that IDE's will show less imports to choose
from, as previously it would also include all subpaths, now IDE's should be able
to unambiguously import from `next-widgets`.

To migrate, change all imports in your application to use `from '@biomedit/next-widgets'`
exclusively.

For example, change the following code:
```
import { Tooltip } from '@biomedit/next-widgets/esm/lib/widgets/Tooltip';
```
into the following:
```
import { Tooltip } from '@biomedit/next-widgets';
```

Signed-off-by: martinfrancois <f.martin@fastmail.com>
* **npm:** material-ui version 4 was replaced with material-ui version 5.

To migrate, [migrate your application to use material-ui version 5](https://mui.com/guides/migration-v4/#migrate-from-jss).
* The styles applied to `UserMenuItem` are now the default styling
of `ListItemAction` and `ListItemButton`.
The `UserMenuItem` component was removed.

The `UserMenuItem` component was merely a styled version of the `ListItemAction`
and `LogoutMenuItem` applied the same styles as `UserMenuItem` but on
`ListItemButton`.
Moving the styling from `UserMenuItem` to `ListItemBase` makes `UserMenuItem`
just an alias to the `ListItemAction` component, making it unnecessary.

Defining the styles applied to `UserMenuItem` as the default style for
`ListItemAction` and `ListItemButton` might lead to unexpected style
changes for users of those components directly, but it can be overwritten if
not desired.

To migrate, use the `ListItemAction` component instead of the `UserMenuItem`
component.
If you are using `ListItemAction` or `ListItemButton` and find the new styling
undesirable, pass the `textClasses` and `iconClasses` props to customize the
styling.
* `StepConnector` is no longer exported.

It does not make sense to use the `StepConnector` component
on its own, but rather to use `Stepper` instead.
There are no disadvantages to using `Stepper`, as
`StepConnector` anyways only makes sense to be used together
with the `Stepper` component.

To migrate, use `Stepper` from `next-widgets` directly instead
of using `StepConnector` in your application in a `Stepper`
from material-ui.

* migrate from JSS to emotion ([65f802b](https://gitlab.com/biomedit/next-widgets/commit/65f802b912f05a2a6a4b109f50608ca215ad8097)), closes [#17](https://gitlab.com/biomedit/next-widgets/issues/17)
* move `UserMenuItem` and `LogoutMenuItem` styles into `ListItemBase` ([4e81861](https://gitlab.com/biomedit/next-widgets/commit/4e81861c9a7097abc803404f2f6e788de9573043)), closes [#17](https://gitlab.com/biomedit/next-widgets/issues/17)


### build

* **npm:** add export map ([61c7439](https://gitlab.com/biomedit/next-widgets/commit/61c743928adb62101b34865bc4af7152176392ad))
* **npm:** remove material-ui 4 dependencies ([63f9936](https://gitlab.com/biomedit/next-widgets/commit/63f99364ece45a6c105a9ce71e498d7761af96ca)), closes [#17](https://gitlab.com/biomedit/next-widgets/issues/17)

## [5.0.0](https://gitlab.com/biomedit/next-widgets/compare/4.6.0...5.0.0) (2021-09-30)


### ⚠ BREAKING CHANGES

* **actionTypes & sagas:** The `apiFunction` argument on the `takeApi` method
(returned by `latest` and `leading` of `takeApiFactory`)
was moved to the `declareAction` method as a mandatory second argument.

Passing the `apiFunction` to the `takeApi` method makes it impossible to perform
type checks when dispatching an action to ensure only properties which are
available can be used, as the `takeApi` method is used for defining the sagas
that are aggregated in a `rootSaga` function, after which the necessary
types cannot be inferred anymore.

Adding the `apiFunction` argument to `declareAction` allows when passing the
resulting `ActionType` to the `requestAction` method to create an action which
restricts typing to only allow request parameters which are available for the
respective `apiFunction`.

To migrate, move the value of the `apiFunction` argument from the `.latest(` and `.leading(`
method calls to the `declareAction` method of the corresponding `ActionType`,
omitting ".request" (to pass in the `ActionType` directly).
Then, change all dispatches of actions related to `ActionType` to create the
action using the `requestAction` method instead of creating an action object
directly.
Make sure to move `onSuccess` if previously used in the action object's properties
to the third argument of the `requestAction` method instead of including it in the
`requestParams`.

For example, change the following code:
```tsx
yield takeApi.latest(LOAD_PROJECTS, api.listProjects),

export const LOAD_PROJECTS = declareAction('LOAD_PROJECTS');

dispatch({ type: LOAD_PROJECTS.request, ordering: 'name', onSuccess: { type: "SUCCESS" } });
```
into the following:
```tsx
yield takeApi.latest(LOAD_PROJECTS),

export const LOAD_PROJECTS = declareAction('LOAD_PROJECTS', api.listProjects);

dispatch(requestAction(LOAD_PROJECTS, { ordering: 'name' }, { type: "SUCCESS" }));
```
* **types:** An object of type `Action` must be provided to the `action` property
in the props of `ListItemAction` and `UserMenuItem` components and the `UserMenuItemModel` type.

Using `ActionType` in the `ListItemAction` and `UserMenuItem` component and the `UserMenuItemModel`
limits the usage to actions which perform API-like functions (which require actions like `_REQUEST`, `_SUCCESS` etc.).
However, this limits the flexibility in which actions can be used, which is why the type was changed to `Action` instead.

To migrate, add to the value of the `action` prop passed to the `ListItemAction` and `UserMenuItem` components and in
the `UserMenuItemModel` type a suffix of `.request`.
For example, change the following code:
```tsx
export const ADD_ACTION = declareAction('ADD_ACTION');
<ListItemAction action={ADD_ACTION} />
```
into the following:
```tsx
export const ADD_ACTION = declareAction('ADD_ACTION');
<ListItemAction action={ADD_ACTION.request} />
```

### Bug Fixes

* **frontend/ListPage:** fix DOM nesting bug on ListPage ([99a6e65](https://gitlab.com/biomedit/next-widgets/commit/99a6e657b6683f70eec38b5a74ee172a731703ab)), closes [#16](https://gitlab.com/biomedit/next-widgets/issues/16)


* **actionTypes & sagas:** improve type safety of redux actions involving API calls ([19a4e82](https://gitlab.com/biomedit/next-widgets/commit/19a4e8255e5465dcbb9ed45fb5433e152c8b94ce)), closes [#2](https://gitlab.com/biomedit/next-widgets/issues/2)
* **types:** change the type of `action` in `ListItemActionProps`, `UserMenuItemProps` and `UserMenuItemModel` from `ActionType` to `Action` ([39a8352](https://gitlab.com/biomedit/next-widgets/commit/39a83525c0aa291cbf3e5525c4a65c7f232476d8)), closes [#2](https://gitlab.com/biomedit/next-widgets/issues/2)

## [4.6.0](https://gitlab.com/biomedit/next-widgets/compare/4.5.0...4.6.0) (2021-09-29)


### Features

* **ListPage:** add a separator between list item's content and action buttons ([17189ee](https://gitlab.com/biomedit/next-widgets/commit/17189ee8c74d6b22a25202ddf5a022b94d71110d))

## [4.5.0](https://gitlab.com/biomedit/next-widgets/compare/4.4.1...4.5.0) (2021-09-24)


### Features

* **Timeline:** add possibility to customize title and message `Typography` components ([dab7e61](https://gitlab.com/biomedit/next-widgets/commit/dab7e61f3867678c278d4cfdf5d488924634d1e4))

### [4.4.1](https://gitlab.com/biomedit/next-widgets/compare/4.4.0...4.4.1) (2021-09-16)


### Bug Fixes

* **frontend/Timeline:** prevent message date to overlap with message icon. ([d1e85a0](https://gitlab.com/biomedit/next-widgets/commit/d1e85a0ccb674cec4536458030f109aa2eff0958))

## [4.4.0](https://gitlab.com/biomedit/next-widgets/compare/4.3.2...4.4.0) (2021-09-13)


### Features

* **Buttons:** add tooltip to `AddIconButton`, `EditIconButton` and `DeleteIconButton` ([f067b75](https://gitlab.com/biomedit/next-widgets/commit/f067b755d1a3eb989307e760af90001aaa3717d4))
* **EnhancedTable:** allow `canEdit` and `canDelete` to be functions ([2abd087](https://gitlab.com/biomedit/next-widgets/commit/2abd0876e14b7414835d5a32043ac0f29b1759eb))

### [4.3.2](https://gitlab.com/biomedit/next-widgets/compare/4.3.1...4.3.2) (2021-09-09)


### Bug Fixes

* **EnhancedTable:** fix a bug where updating `canEdit` or `canDelete` after the first render would NOT have an effect ([cb27cb0](https://gitlab.com/biomedit/next-widgets/commit/cb27cb09ec6baeb5aa7aea3fb6662e1ed2e91226))

### [4.3.1](https://gitlab.com/biomedit/next-widgets/compare/4.3.0...4.3.1) (2021-08-20)

## [4.3.0](https://gitlab.com/biomedit/next-widgets/compare/4.2.0...4.3.0) (2021-08-18)


### Features

* **Description:** allow styling of 'Description' ([a8cd401](https://gitlab.com/biomedit/next-widgets/commit/a8cd401ee8cd8bd88c38d870c6aa3d38e9183077))
* **PageBase:** title could be specified as array ([38c3ffe](https://gitlab.com/biomedit/next-widgets/commit/38c3ffea063f68c705466800e4e5f279d7d8c303))

## [4.2.0](https://gitlab.com/biomedit/next-widgets/compare/4.1.0...4.2.0) (2021-08-17)


### Features

* **Buttons:** add "Archive" and "Unarchive" icon buttons ([7f56d8c](https://gitlab.com/biomedit/next-widgets/commit/7f56d8c37edfc26be0820154a5b753280fd3b2b8))
* **DeleteDialog:** prompt user to type the name in a text field to confirm deletion when `deleteItem` is specified ([e7e4548](https://gitlab.com/biomedit/next-widgets/commit/e7e4548a1068fb140cd200df1a9cb1357a2840d0))
* **ListPage:** allow adding additional action buttons to `ListPage` ([361c74f](https://gitlab.com/biomedit/next-widgets/commit/361c74f343f52937435e3dbee87c122d5e9f2b85))

## [4.1.0](https://gitlab.com/biomedit/next-widgets/compare/4.0.1...4.1.0) (2021-08-05)


### Features

* **Markdown:** open links in markdown in a new tab when the prop `openLinksInNewTab` is set to `true` ([2f229bd](https://gitlab.com/biomedit/next-widgets/commit/2f229bd4b89688d74897139af2f66791758d84b0))


### Bug Fixes

* **SelectField:** show required error when losing focus without selection ([8136a76](https://gitlab.com/biomedit/next-widgets/commit/8136a769263dbb2b5afd42623d75dc0fbb88ef5a))
* **SelectField:** show select control in red in case of an error ([a9def18](https://gitlab.com/biomedit/next-widgets/commit/a9def18a38555869007bbb38f96ceae693301e56))

### [4.0.1](https://gitlab.com/biomedit/next-widgets/compare/4.0.0...4.0.1) (2021-07-19)

## [4.0.0](https://gitlab.com/biomedit/next-widgets/compare/3.4.1...4.0.0) (2021-07-19)


### ⚠ BREAKING CHANGES

* **EnhancedTable:** `@material-ui/core` version 4.12.0 included a
* **EnhancedTable:** in the types, which means this version is affected as well.

To migrate, upgrade the version of `@material-ui/core` in your `package.json` to at least **4.12.1**.
In order to use a version of `@material-ui/core` **prior to 4.12.0**, use `next-widgets` version **3.4.0 or lower**.

### Bug Fixes

* **EnhancedTable:** apply changes to prop name due to changes after upgrading to `@material-ui/core` version 4.12.1 ([5c896f4](https://gitlab.com/biomedit/next-widgets/commit/5c896f4eeddc3e0ea4e7a4546c9e8b1fbbf59396)), closes [/github.com/mui-org/material-ui/pull/23789#issuecomment-876249753](https://github.com/mui-org/material-ui/pull/23789/issues/issuecomment-876249753)

### [3.4.1](https://gitlab.com/biomedit/next-widgets/compare/3.4.0...3.4.1) (2021-07-19)


### Bug Fixes

* **SelectField:** prevent error from occurring when a `value` is used in a `SelectField` which doesn't exist in `choices` ([6fc7a32](https://gitlab.com/biomedit/next-widgets/commit/6fc7a32109cde1af27e33e6d8d1af270e3c669e5))

## [3.4.0](https://gitlab.com/biomedit/next-widgets/compare/3.3.0...3.4.0) (2021-07-16)


### Features

* **testUtils:** add method `expectToBeInTheDocument` ([416c79a](https://gitlab.com/biomedit/next-widgets/commit/416c79a1d2dca66b164dabb6b2dc279c8d5ec951))

## [3.3.0](https://gitlab.com/biomedit/next-widgets/compare/3.2.1...3.3.0) (2021-07-12)


### Features

* **sortTypes:** add support for `accessor` with array values when using it together with `sortType: caseInsensitive` ([ed70cbe](https://gitlab.com/biomedit/next-widgets/commit/ed70cbe1fa62504f1613e0ac53bac3aacb01e23e))

### [3.2.1](https://gitlab.com/biomedit/next-widgets/compare/3.2.0...3.2.1) (2021-07-05)


### Bug Fixes

* **testUtils/RequestVerifier:** always show all requests if an assertion fails ([c3efbf2](https://gitlab.com/biomedit/next-widgets/commit/c3efbf2157c6e394f06b1b47ed2d9d81ff440f68))

## [3.2.0](https://gitlab.com/biomedit/next-widgets/compare/3.1.0...3.2.0) (2021-07-02)


### Features

* **testUtils:** add method `fillTextboxes` ([4575b1a](https://gitlab.com/biomedit/next-widgets/commit/4575b1a49920780ba23c9b15df40d462880361ab))

## [3.1.0](https://gitlab.com/biomedit/next-widgets/compare/3.0.1...3.1.0) (2021-07-02)


### Features

* **testUtils:** support mocking the `Trans` component as well with `mockI18n` ([96935d0](https://gitlab.com/biomedit/next-widgets/commit/96935d089e72ecf8dc93c9791d4203e5f130a7c8))

### [3.0.1](https://gitlab.com/biomedit/next-widgets/compare/3.0.0...3.0.1) (2021-07-01)

## [3.0.0](https://gitlab.com/biomedit/next-widgets/compare/2.5.6...3.0.0) (2021-07-01)


### ⚠ BREAKING CHANGES

* **utils:** Since required parameters cannot follow optional parameters (TS1016) and `email` is a required parameter,
it wouldn't be possible to make this change without changing the order of the parameters to have the `email` as the first
parameter.
An alternative would've been to make the `emaiL` address optional as well, but since it doesn't really make sense to have
a `mailto` without an email address, this doesn't make sense.

To migrate, change all calls to `mailto` to put the `email` parameter first, instead of last.
The new signature is the following: `email: string, subject?: string, body?: string`

### Features

* **utils:** make `subject` and `body` of the `mailto` function optional ([b53fc05](https://gitlab.com/biomedit/next-widgets/commit/b53fc05954ecb420fa8c3705952a27e76af59c64))
* **widgets:** add `EmailLink` component ([60e41f0](https://gitlab.com/biomedit/next-widgets/commit/60e41f0f36c665784d52e19ff9372b5b27069e57))

### [2.5.6](https://gitlab.com/biomedit/next-widgets/compare/2.5.5...2.5.6) (2021-07-01)

### [2.5.5](https://gitlab.com/biomedit/next-widgets/compare/2.5.4...2.5.5) (2021-07-01)

### [2.5.4](https://gitlab.com/biomedit/next-widgets/compare/2.5.3...2.5.4) (2021-06-28)


### Bug Fixes

* **CheckboxField:** remove materialui warning about `classes` receiving prop of type `boolean` ([4cecd1b](https://gitlab.com/biomedit/next-widgets/commit/4cecd1bc289aa1d49300a64d70cb11aac6550132))

### [2.5.3](https://gitlab.com/biomedit/next-widgets/compare/2.5.2...2.5.3) (2021-06-28)

### [2.5.2](https://gitlab.com/biomedit/next-widgets/compare/2.5.1...2.5.2) (2021-06-25)

### [2.5.1](https://gitlab.com/biomedit/next-widgets/compare/2.5.0...2.5.1) (2021-06-25)

## [2.5.0](https://gitlab.com/biomedit/next-widgets/compare/2.4.0...2.5.0) (2021-06-25)


### Features

* **ExternalLink:** add widget to make an external link which opens in a new tab by default ([1c246e2](https://gitlab.com/biomedit/next-widgets/commit/1c246e2c958a0c5c674040307234a8f5d100d4bc))

## [2.4.0](https://gitlab.com/biomedit/next-widgets/compare/2.3.0...2.4.0) (2021-06-25)


### Features

* **CheckboxField:** add visual feedback when required validation fails ([299f532](https://gitlab.com/biomedit/next-widgets/commit/299f532f47b194c6cba1badf74250e899045190f))

## [2.3.0](https://gitlab.com/biomedit/next-widgets/compare/2.2.0...2.3.0) (2021-06-25)


### Features

* **CheckboxField:** enable validating a `CheckboxField` to be `required` ([3ab66b9](https://gitlab.com/biomedit/next-widgets/commit/3ab66b9bac0e0629cddf3130bff9abeb5ccf8224))

## [2.2.0](https://gitlab.com/biomedit/next-widgets/compare/2.1.0...2.2.0) (2021-06-24)


### Features

* **sagas:** allow to define error status codes (4xx - 5xx) which are handled in `takeApi` as an additional optional parameter ([8776877](https://gitlab.com/biomedit/next-widgets/commit/87768777704ab13e49233ab8d9024da82201ecbc))

## [2.1.0](https://gitlab.com/biomedit/next-widgets/compare/2.0.1...2.1.0) (2021-06-23)


### Features

* **FixedChildrenHeight:** forward other props which are passed in to `Box` ([4c552ac](https://gitlab.com/biomedit/next-widgets/commit/4c552ac8284d872a6d4ab322d25c4816fc0b181d))

### [2.0.1](https://gitlab.com/biomedit/next-widgets/compare/2.0.0...2.0.1) (2021-06-22)


### Bug Fixes

* circular dependencies ([93d005e](https://gitlab.com/biomedit/next-widgets/commit/93d005e022f624f390ae1db1bae567942ac7350b))

## [2.0.0](https://gitlab.com/biomedit/next-widgets/compare/1.1.1...2.0.0) (2021-06-21)


### ⚠ BREAKING CHANGES

* The `getTestFileAsString` method is very specific to a setup and
depends on the folder structure of `node_modules`, if the method is not used in
the app itself.
Therefore it doesn't make sense to include it in a library, but to keep it in the
apps themselves.

To migrate, copy the implementation to your own code.

* remove `getTestFileAsString` method ([ab98258](https://gitlab.com/biomedit/next-widgets/commit/ab98258bc093d022db5f06f0502c1715c66a02e2))

### [1.1.1](https://gitlab.com/biomedit/next-widgets/compare/1.1.0...1.1.1) (2021-06-21)

## [1.1.0](https://gitlab.com/biomedit/next-widgets/compare/1.0.1...1.1.0) (2021-06-18)


### Features

* **EnhancedTable:** allow disabling filtering and pagination ([fce58e1](https://gitlab.com/biomedit/next-widgets/commit/fce58e16398d7483c48f3d33105351709d42d76c))

### 1.0.1 (2021-06-11)
