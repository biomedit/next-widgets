#!/usr/bin/env bash

set -e

# Set default values.
dry_run=""
CLIFF_CONFIG=".cliff.toml"
CHANGELOG_FILE="CHANGELOG.md"
FILES_TO_COMMIT=(package.json package-lock.json "$CHANGELOG_FILE")
NODE_PKG_VERSION_SCRIPT="console.log(JSON.parse(require('fs').readFileSync(process.argv[1]))['version'])"
DOCSTRING="BIWG's version bump script for new next-widgets releases.

DESCRIPTION:
  Creates a new commit in the repo that contains changes for the upcoming
  new release. The updates to $CHANGELOG_FILE are made using git cliff.
  Specifically:
  * Updates $CHANGELOG_FILE for the new release.
  * Adds a new release commit with the updated version of $CHANGELOG_FILE.
  * Tags the new release commit with the upcoming version to be released.

USAGE:
  $(basename "$0") [OPTIONS]

  OPTIONS:
   -d / --dry-run: run script without making changes to files and commits.
   -h / --help: display this help message.

EXAMPLES:
  bumpversion.sh -d
"

# The `sed` in-place command behaves differently between the BSD (MacOS) and GNU (Linux)
# implementations. This makes the command portable.
# Default case for Linux sed, just use "-i"
sedi=(-i)
case "$(uname)" in
  # For MacOS, use two parameters
  Darwin*) sedi=(-i "") ;;
esac

# Get command line arguments passed by the user.
while [ "$1" ]; do
  case $1 in
    -d | --dry-run)
      dry_run=TRUE
      ;;
    -h | h | [-]*help)
      echo "$DOCSTRING"
      exit 1
      ;;
    *)
      echo "💥 Bad argument. Please see --help for details." >&2
      exit 1
      ;;
  esac
  shift
done

# Get the current version.
version_current=$(node -e "${NODE_PKG_VERSION_SCRIPT}" -- package.json)
dryrun_label="🔍 dry-run mode"
echo "📌 Bumping version for next-widgets $(if [ $dry_run ]; then echo " [$dryrun_label]"; fi)" >&2
echo "📌 Current version: $version_current" >&2

# Calculate the next version number
version_new=$(git cliff -c "$CLIFF_CONFIG" --bumped-version)
if [ "$version_new" = "$version_current" ]; then
  echo "🚨 No changes detected, skipping version bump" >&2
  exit 0
fi
echo "📦 Bumping to version $version_new" >&2

npm version --no-git-tag-version "$version_new" >/dev/null

# Append content for the next release to the CHANGELOG file.
echo "📜 Generating changelog" >&2
echo "----------------------------------------------------------------------------------------------------" >&2
# Print the changelog to stdout
git cliff -c "$CLIFF_CONFIG" -u --tag "$version_new" >&2
echo "----------------------------------------------------------------------------------------------------" >&2
git cliff -c "$CLIFF_CONFIG" -u -p "$CHANGELOG_FILE" --tag "$version_new" >&2

# Add new commit and tag to the repo.
commit_msg="chore(release): ${version_new}"
if [ ! $dry_run ]; then
  git add "${FILES_TO_COMMIT[@]}"
  git commit -q -m "$commit_msg"
  git tag -a "$version_new" -m "$commit_msg"
else
  git restore "${FILES_TO_COMMIT[@]}"
fi
echo "🖊️  New commit: $commit_msg" >&2
echo "🏷️  New tag: $version_new" >&2
echo "🚀 Version bumped to: $version_new" >&2

# Print end-message to user.
if [ ! $dry_run ]; then
  echo "👷 You can now push the new version using: git push --follow-tags origin $(git branch --show-current)" >&2
else
  echo "$dryrun_label: completed test run." >&2
fi

# Print the new version on stdout, in case the user wants to retrieve this
# value for further usage by the shell.
echo "$version_new"
