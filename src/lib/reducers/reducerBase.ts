import { ActionType, AnyActionType } from '../actions/actionTypes';
import { IdType } from '../../types';
import { produce } from 'immer';

type ExtractRequestParams<P> =
  P extends ActionType<infer RequestParams, never> ? RequestParams : never;

type ExtractResponseType<P> =
  P extends ActionType<never, infer ResponseType> ? ResponseType : never;

type ItemIdType = number | string | undefined;

export type ApiAction<
  A extends ActionType<RequestParams, ResponseType>,
  RequestParams extends object = ExtractRequestParams<A>,
  ResponseType = ExtractResponseType<A>,
> =
  | {
      error: unknown;
      type: `${A['failure']}`;
    }
  | {
      requestArgs?: RequestParams;
      response?: ResponseType;
      type: `${A['success']}`;
    }
  | {
      type: `${A['request']}`;
    };

export type BaseReducerState<T extends IdType> = {
  isFetching: boolean;
  isSubmitting: boolean;
  itemList: T[];
};

export function initialBaseReducerState<
  T extends IdType,
>(): BaseReducerState<T> {
  return {
    isFetching: false,
    isSubmitting: false,
    itemList: [],
  };
}

type BaseReducerArgs = {
  add?: AnyActionType;
  clear?: string;
  del?: AnyActionType;
  load?: AnyActionType;
  retrieve?: AnyActionType;
  update?: AnyActionType;
};

export function reducer<T extends IdType>({
  load,
  del,
  update,
  add,
  clear,
  retrieve,
}: BaseReducerArgs) {
  return (
    state: BaseReducerState<T> = initialBaseReducerState<T>(),
    action: ApiAction<ActionType<IdType, T | T[]>>,
  ): BaseReducerState<T> =>
    produce(state, (draft: BaseReducerState<T>) => {
      switch (action.type) {
        case load?.request:
          draft.isFetching = true;
          break;
        case load?.success: {
          draft.isFetching = false;
          if (load?.success && action.response) {
            draft.itemList = action.response as T[];
          } else {
            draft.itemList = initialBaseReducerState<T>().itemList;
          }
          break;
        }
        case load?.failure:
          draft.isFetching = false;
          break;
        case retrieve?.request:
          draft.isFetching = true;
          break;
        case retrieve?.success: {
          draft.isFetching = false;
          if (retrieve?.success && action.response) {
            draft.itemList = [action.response] as T[];
          } else {
            draft.itemList = initialBaseReducerState<T>().itemList;
          }
          break;
        }
        case retrieve?.failure:
          draft.isFetching = false;
          break;
        case del?.request:
          draft.isSubmitting = true;
          break;
        case del?.success: {
          draft.isSubmitting = false;
          const deletedItemId: ItemIdType = action?.requestArgs?.id;
          draft.itemList = draft.itemList.filter(
            (item) => String(item['id']) !== String(deletedItemId),
          );
          break;
        }
        case del?.failure:
          draft.isSubmitting = false;
          break;
        case update?.request:
          draft.isSubmitting = true;
          break;
        case update?.success: {
          draft.isSubmitting = false;
          if (action.response) {
            const updatedItemId: ItemIdType = (action.response as T)?.['id'];
            if (updatedItemId !== undefined) {
              const updatedItemIndex = draft.itemList.findIndex(
                (item) => String(item['id']) === String(updatedItemId),
              );
              draft.itemList[updatedItemIndex] = action.response as T;
            } else {
              console.error('Response does NOT include an id!');
            }
          } else {
            console.error('Response does NOT include updated item!');
          }
          break;
        }
        case update?.failure:
          draft.isSubmitting = false;
          break;
        case add?.request:
          draft.isSubmitting = true;
          break;
        case add?.success: {
          draft.isSubmitting = false;
          if (action.response) {
            draft.itemList.push(action.response as T);
          } else {
            console.error('Response does NOT include added item!');
          }
          break;
        }
        case add?.failure:
          draft.isSubmitting = false;
          break;
        case clear:
          draft.itemList = [];
          break;
      }
    });
}
