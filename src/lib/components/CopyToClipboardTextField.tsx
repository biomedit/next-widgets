import React, { useCallback, useState } from 'react';

import TextField, { TextFieldProps } from '@mui/material/TextField';
import ContentCopy from '@mui/icons-material/ContentCopy';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';

import { ToastBarSnackBar } from '../widgets';
import { ToastSeverity } from '../reducers';

type CopyToClipboardTextFieldProps = Omit<TextFieldProps, 'defaultValue'> & {
  iconButtonTitle?: string;
  value: string | undefined;
};

export const useCopyToClipboardState = (): {
  onClick: (value: string | undefined) => void;
  onClose: () => void;
  open: boolean;
} => {
  const [open, setOpen] = useState(false);
  const onClick = useCallback((value: string | undefined) => {
    navigator.clipboard.writeText(value ?? '');
    setOpen(true);
  }, []);
  return {
    open,
    onClose: useCallback(() => setOpen(false), []),
    onClick,
  };
};

export const CopyToClipboardSnackbar = ({
  open,
  onClose,
}: {
  onClose: () => void;
  open: boolean;
}) => {
  return (
    <ToastBarSnackBar
      open={open}
      onClose={onClose}
      severity={ToastSeverity.SUCCESS}
    >
      <span>Copied to clipboard</span>
    </ToastBarSnackBar>
  );
};

export const ContentCopyButton = ({ onClick }: { onClick: () => void }) => {
  return (
    <IconButton
      color="primary"
      aria-label="Copy to clipboard"
      title="Copy to clipboard"
      onClick={onClick}
      size="medium"
    >
      <ContentCopy fontSize="small" />
    </IconButton>
  );
};

export const CopyToClipboardTextField = ({
  value,
  ...props
}: CopyToClipboardTextFieldProps) => {
  const { open, onClose, onClick } = useCopyToClipboardState();
  return (
    <>
      <TextField
        defaultValue={value}
        margin="normal"
        size="small"
        {...props}
        slotProps={{
          input: {
            readOnly: true,
            endAdornment: (
              <InputAdornment position="end">
                <ContentCopyButton onClick={() => onClick(value)} />
              </InputAdornment>
            ),
          },
        }}
      />
      <CopyToClipboardSnackbar open={open} onClose={onClose} />
    </>
  );
};
