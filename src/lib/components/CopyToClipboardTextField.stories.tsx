import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { CopyToClipboardTextField } from './CopyToClipboardTextField';

export default {
  title: 'Components/CopyToClipboardTextField',
  component: CopyToClipboardTextField,
} as Meta<typeof CopyToClipboardTextField>;

const Template: StoryFn<typeof CopyToClipboardTextField> = (args) => (
  <CopyToClipboardTextField {...args} value="Cogito ergosum" />
);

export const Default = Template.bind({});
