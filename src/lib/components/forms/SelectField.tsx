import React, { ReactElement, useCallback, useMemo } from 'react';

import { CircularProgress, ListItemText } from '@mui/material';
import Select, { SelectProps } from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import { styled } from '@mui/material/styles';

import { Choice, choicesByValue } from '../choice';
import { Control, Controller, FieldValues } from 'react-hook-form';
import {
  EnhancedArrayFieldProps,
  EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
  UseEnhancedFieldHookReturn,
} from './hooks';
import { joinIfArray } from '../../utils';
import { required } from './validators';

type MultipleSelectItemProps = {
  readonly choice: Choice;
  readonly value: string[];
};

const MultipleSelectItem = ({ value, choice }: MultipleSelectItemProps) => {
  return (
    <>
      <Checkbox checked={!!value.find((it) => it === choice.value)} />
      <ListItemText primary={choice.label} />
    </>
  );
};

export type SelectFieldBaseProps = EnhancedFieldProps &
  Pick<SelectProps, 'inputProps'> &
  UseEnhancedFieldHookReturn & {
    choices: Choice[];
    className?: string;
    control?: Control;
    disabled?: boolean;
    fullWidth?: boolean;
    helperText?: string;
    isLoading?: boolean;
    label: string;
    multiple?: boolean;
    required?: boolean;
    testId?: string;
  };

const StyledFormControl = styled(FormControl)({
  minWidth: 250,
  paddingBottom: 20,
});

export function SelectFieldBase(props: SelectFieldBaseProps): ReactElement {
  const {
    name,
    label,
    error,
    control,
    defaultValue,
    choices,
    disabled,
    isLoading,
    className,
    multiple,
    testId,
    fullWidth,
    helperText,
    ...selectProps
  } = props;

  const byValue = useMemo(() => {
    return choicesByValue(choices);
  }, [choices]);

  const toLabelString = useCallback(
    (selected: string[] | string) => {
      const arr = Array.isArray(selected) ? selected : [selected];
      const labels = arr
        .filter((sel: string) => sel in byValue)
        .map((sel: string) => byValue[sel].label);
      return joinIfArray(labels, ', ');
    },
    [byValue],
  );

  if (isLoading) {
    return <CircularProgress />;
  }

  return (
    <StyledFormControl
      key={'select-formcontrol-' + label}
      className={className}
      error={!!error?.message}
      fullWidth={fullWidth}
    >
      <InputLabel
        key={'select-inputlabel-' + label}
        id={'select-inputlabel-' + label}
        size="small"
      >
        {label}
      </InputLabel>
      <Controller
        render={({ field: { value, onChange, onBlur } }) => {
          return (
            <Select
              size="small"
              labelId={'select-inputlabel-' + label}
              label={label}
              key={'select-' + label}
              type="text"
              disabled={disabled}
              multiple={multiple}
              value={value || (multiple ? [] : '')}
              onChange={onChange}
              onBlur={onBlur}
              renderValue={toLabelString}
              inputProps={{ ...selectProps.inputProps, 'data-testid': testId }}
              {...selectProps}
            >
              {choices.map((choice) => (
                <MenuItem
                  key={'select-menuitem-' + choice.key}
                  value={choice.value}
                >
                  {multiple ? (
                    <MultipleSelectItem
                      value={value as string[]}
                      choice={choice}
                    />
                  ) : (
                    choice.label
                  )}
                </MenuItem>
              ))}
            </Select>
          );
        }}
        name={name}
        defaultValue={defaultValue}
        control={control}
        rules={props.required ? required : undefined}
      />
      <FormHelperText
        key={'select-formhelpertext-' + label}
        error={!!error?.message}
      >
        {error?.message || helperText}
      </FormHelperText>
    </StyledFormControl>
  );
}

export type SelectFieldProps<TInitialValues extends FieldValues = FieldValues> =
  EnhancedFieldProps<TInitialValues> &
    Omit<SelectFieldBaseProps, 'control' | 'defaultValue' | 'error'>;

export function SelectField<TInitialValues extends FieldValues = FieldValues>({
  name,
  initialValues,
  populateIfSingleChoice,
  choices,
  ...props
}: SelectFieldProps<TInitialValues>): ReactElement {
  const { defaultValue, error, control } = useEnhancedField<TInitialValues>({
    name,
    initialValues,
    populateIfSingleChoice,
    choices,
  });

  return (
    <SelectFieldBase
      choices={choices}
      name={name}
      defaultValue={defaultValue}
      error={error}
      control={control}
      {...props}
    />
  );
}

export type SelectArrayFieldProps = EnhancedArrayFieldProps &
  Omit<SelectFieldBaseProps, 'control' | 'defaultValue' | 'error' | 'name'>;

export function SelectArrayField({
  arrayName,
  fieldName,
  field,
  index,
  ...props
}: SelectArrayFieldProps): ReactElement {
  const { error, name, defaultValue, control } = useEnhancedArrayField({
    arrayName,
    fieldName,
    field,
    index,
  });

  return (
    <SelectFieldBase
      error={error}
      name={name}
      defaultValue={defaultValue}
      control={control}
      {...props}
    />
  );
}
