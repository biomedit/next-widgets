import {
  FieldArrayPath,
  FieldArrayWithId,
  FieldError,
  FieldValues,
  useForm,
  useFormContext,
  UseFormProps,
  UseFormReturn,
} from 'react-hook-form';
import { Choice } from '../choice';
import get from 'lodash/get';

/**
 * Specifies some default options all of the forms should have,
 * without having to pass them explicitly to all of the usages of `useForm` from `react-hook-form`.
 * All of the options can be overridden in {@code formOptions}.
 * @param formOptions additional options to set, as passed when using `useForm` directly
 */
export function useEnhancedForm<
  TFieldValues extends FieldValues = FieldValues,
  TContext extends Record<string, unknown> = Record<string, unknown>,
>(
  formOptions?: UseFormProps<TFieldValues, TContext>,
): UseFormReturn<TFieldValues, TContext> {
  const defaultOptions: UseFormProps<TFieldValues, TContext> = {
    mode: 'onTouched',
  };

  return useForm<TFieldValues, TContext>({ ...defaultOptions, ...formOptions });
}

export type EnhancedArrayFieldProps<
  TFieldValues extends FieldValues = FieldValues,
  TFieldArrayName extends
    FieldArrayPath<TFieldValues> = FieldArrayPath<TFieldValues>,
  TKeyName extends string = 'key',
> = {
  arrayName: string;
  field: Partial<FieldArrayWithId<TFieldValues, TFieldArrayName, TKeyName>>;
  fieldName: string;
  index: number;
};

export function useEnhancedArrayField<
  TFieldValues extends FieldValues = FieldValues,
  TFieldArrayName extends
    FieldArrayPath<TFieldValues> = FieldArrayPath<TFieldValues>,
  TKeyName extends string = 'key',
>(props: EnhancedArrayFieldProps<TFieldValues, TFieldArrayName, TKeyName>) {
  const { arrayName, field, fieldName, index } = props;

  const methods = useFormContext<TFieldValues>();
  const {
    formState: { errors },
  } = methods;

  const name = `${arrayName}[${index}].${fieldName}`;
  const defaultValue = get(field, fieldName);
  const error: FieldError = get(errors, [arrayName, index, fieldName]);

  return { ...methods, name, defaultValue, error };
}

export type UseEnhancedFieldHookReturn = {
  defaultValue?: Date | number[] | string[] | number | string | unknown;
  error: FieldError;
};

export interface UseEnhancedFieldHook {
  <TInitialValues extends FieldValues = FieldValues>(
    props: EnhancedFieldProps<TInitialValues>,
  ): ReturnType<typeof useFormContext> & UseEnhancedFieldHookReturn;
}

export type EnhancedFieldProps<
  TInitialValues extends FieldValues = FieldValues,
> = {
  choices?: Choice[];
  initialValues?: TInitialValues;
  name: string;
  populateIfSingleChoice?: boolean;
};

export const useEnhancedField: UseEnhancedFieldHook = ({
  name,
  initialValues,
  populateIfSingleChoice,
  choices,
}: EnhancedFieldProps) => {
  const methods = useFormContext();
  const {
    formState: { errors },
  } = methods;
  const defaultValue =
    get(initialValues, name) ??
    (populateIfSingleChoice && choices?.length === 1 ? choices[0].value : '');
  const error = get(errors, name) as FieldError;

  return { ...methods, defaultValue, error };
};
