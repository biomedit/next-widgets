import React, { ReactElement, ReactNode } from 'react';

import {
  Autocomplete,
  AutocompleteProps,
  TextField,
  TextFieldProps,
} from '@mui/material';
import { AnyObject } from '../../../types';
import { FieldError } from 'react-hook-form';
import { styled } from '@mui/material/styles';

export interface AutocompleteOption extends AnyObject {
  label: string;
}

export type AutoSelectBaseProps<T> = Pick<
  AutocompleteProps<T, false, true, false>,
  'classes' | 'disabled' | 'readOnly' | 'sx'
> &
  Pick<TextFieldProps, 'label' | 'placeholder'> & {
    choices: Array<T>;
    error?: Partial<FieldError>;
    fields: Array<string>;
    inputValue?: string;
    onChange: (value: T | null) => void;
    onInputChange?: (value: string) => void;
  };

const StyledCell = styled('div')(({ theme }) => ({
  padding: theme.spacing(2),
  display: 'table-cell',
}));

export function AutoSelectBase<T extends AutocompleteOption>({
  label,
  placeholder,
  choices,
  fields,
  onChange,
  error,
  inputValue,
  onInputChange,
  ...other
}: AutoSelectBaseProps<T>): ReactElement {
  const renderOption = (
    props: React.HTMLAttributes<HTMLElement>,
    option: T,
  ) => {
    return (
      <div {...props} style={{ display: 'table-row' }}>
        {fields.map((field) => (
          <StyledCell key={option.label + '-' + field}>
            {option[field] as ReactNode}
          </StyledCell>
        ))}
      </div>
    );
  };

  const getOptionLabel = (option: T): string => option.label || '';

  return (
    <Autocomplete<T, false, true, false>
      disableClearable={true}
      renderInput={(params) => (
        <TextField
          {...params}
          placeholder={placeholder}
          label={label}
          size="small"
          error={!!error}
          helperText={error?.message}
        />
      )}
      isOptionEqualToValue={(option, value) =>
        getOptionLabel(option) === getOptionLabel(value)
      }
      options={choices}
      onChange={(_, value) => onChange(value)}
      renderOption={renderOption}
      getOptionLabel={getOptionLabel}
      onInputChange={(_, value, reason) => {
        if (onInputChange && reason !== 'reset') {
          onInputChange(value);
        }
      }}
      inputValue={inputValue}
      {...other}
    />
  );
}
