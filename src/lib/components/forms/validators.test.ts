import {
  httpsUrl,
  ID_REGEX,
  IP_ADDRESS_REGEX,
  mustBe,
  mustBeOneOf,
  NAME_REGEX,
  PGP_FINGERPRINT_REGEX,
  url,
  USERNAME_REGEX,
} from './validators';

describe('validators', () => {
  describe('regex', () => {
    it.each`
      input                                                 | isValid  | regex
      ${'CHRISTIANR'}                                       | ${false} | ${USERNAME_REGEX}
      ${'foo-bar'}                                          | ${false} | ${USERNAME_REGEX}
      ${'chri'}                                             | ${false} | ${USERNAME_REGEX}
      ${'christianr'}                                       | ${true}  | ${USERNAME_REGEX}
      ${'abcdefghijklmnop'}                                 | ${true}  | ${USERNAME_REGEX}
      ${'abcdefghijklmnopq'}                                | ${false} | ${USERNAME_REGEX}
      ${'ululu'}                                            | ${false} | ${USERNAME_REGEX}
      ${'ululul'}                                           | ${true}  | ${USERNAME_REGEX}
      ${'0123456789ABCDEF0123456789ABCDEF01234567'}         | ${true}  | ${PGP_FINGERPRINT_REGEX}
      ${'0123456789ABCDEF'}                                 | ${false} | ${PGP_FINGERPRINT_REGEX}
      ${'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF'} | ${false} | ${PGP_FINGERPRINT_REGEX}
      ${'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123'}         | ${false} | ${PGP_FINGERPRINT_REGEX}
      ${'Chuck_Nörriß-1940+'}                               | ${true}  | ${NAME_REGEX}
      ${'Roundhouse Kick!'}                                 | ${false} | ${NAME_REGEX}
      ${'chuck_norris_1940'}                                | ${true}  | ${ID_REGEX}
      ${'Chuck_Norris_1940'}                                | ${false} | ${ID_REGEX}
      ${'chuck-norris-1940'}                                | ${false} | ${ID_REGEX}
      ${'127.0.0.1'}                                        | ${true}  | ${IP_ADDRESS_REGEX}
      ${'127.O.O.1'}                                        | ${false} | ${IP_ADDRESS_REGEX}
      ${'127.0.0.1/32'}                                     | ${false} | ${IP_ADDRESS_REGEX}
      ${'127.0.0.2555'}                                     | ${false} | ${IP_ADDRESS_REGEX}
      ${'1277.0.0.1'}                                       | ${false} | ${IP_ADDRESS_REGEX}
      ${'127.0.1'}                                          | ${false} | ${IP_ADDRESS_REGEX}
    `(
      'should validate input "$input" as $isValid',
      ({ input, isValid, regex }) => {
        expect(regex.test(input)).toEqual(isValid);
      },
    );
  });

  describe('custom validators', () => {
    describe('mustBe', () => {
      const target = 'correct';
      const errorMessage = 'Must be ' + target;

      it.each`
        input      | isValid
        ${target}  | ${true}
        ${'wrong'} | ${false}
        ${''}      | ${true}
      `(
        'should validate input "$input" as $isValid',
        async ({ input, isValid }) => {
          if (isValid) {
            expect(mustBe(target).validate(input)).toEqual(isValid);
          } else {
            expect(mustBe(target).validate(input)).toEqual(errorMessage);
          }
        },
      );
    });

    describe('mustBeOneOf', () => {
      const targets = ['these', 'are', 'correct', 'été'];
      const errorMessage = 'Must be one of these, are, correct, été';

      it.each`
        input         | isValid
        ${targets[0]} | ${true}
        ${'wrong'}    | ${false}
        ${''}         | ${true}
        ${'these'}    | ${true}
        ${'THESE'}    | ${true}
        ${'ete'}      | ${false}
        ${'ÉTé'}      | ${true}
      `(
        'should validate input "$input" as $isValid',
        async ({ input, isValid }) => {
          if (isValid) {
            expect(mustBeOneOf(targets).validate(input)).toEqual(isValid);
          } else {
            expect(mustBeOneOf(targets).validate(input)).toEqual(errorMessage);
          }
        },
      );
    });

    describe('url', () => {
      const errorMessage = `Must be a valid URL`;

      it.each`
        input                      | isValid
        ${'https://chuck.norris'}  | ${true}
        ${'https:///chuck.norris'} | ${true}
        ${'http://chuck.norris'}   | ${true}
        ${'ftp://chuck.norris'}    | ${true}
        ${'sftp://chuck.norris'}   | ${true}
        ${'ftps://chuck.norris'}   | ${true}
        ${'ssh://chuck.norris'}    | ${true}
        ${'https//chuck.norris'}   | ${false}
        ${'https:/chuck.norris'}   | ${true}
        ${''}                      | ${true}
      `(
        'should validate input "$input" as $isValid',
        async ({ input, isValid }) => {
          if (isValid) {
            expect(url.validate?.url(input)).toEqual(isValid);
          } else {
            expect(url.validate?.url(input)).toEqual(errorMessage);
          }
        },
      );
    });

    describe('httpsUrl', () => {
      const errorMessage = `Must be a valid HTTPS URL`;

      it.each`
        input                      | isValid
        ${'https://chuck.norris'}  | ${true}
        ${'https:///chuck.norris'} | ${true}
        ${'http://chuck.norris'}   | ${false}
        ${'ftp://chuck.norris'}    | ${false}
        ${'sftp://chuck.norris'}   | ${false}
        ${'ftps://chuck.norris'}   | ${false}
        ${'ssh://chuck.norris'}    | ${false}
        ${'https//chuck.norris'}   | ${false}
        ${'https:/chuck.norris'}   | ${true}
        ${'htps://chuck.norris'}   | ${false}
        ${''}                      | ${true}
      `(
        'should validate input "$input" as $isValid',
        async ({ input, isValid }) => {
          if (isValid) {
            expect(httpsUrl.validate?.httpsUrl(input)).toEqual(isValid);
          } else {
            expect(httpsUrl.validate?.httpsUrl(input)).toEqual(errorMessage);
          }
        },
      );
    });
  });
});
