import * as React from 'react';
import { ReactElement } from 'react';

import {
  fireEvent,
  render,
  renderHook,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import { FormProvider, UseFormReturn } from 'react-hook-form';
import { LabelledField, LabelledFieldProps } from './LabelledField';
import { AnyObject } from '../../../types';
import { mockConsoleError } from '../../testUtils';
import { StatusCodes } from 'http-status-codes';
import { useEnhancedForm } from './hooks';

const testName = 'username';
const testLabel = 'Username';

type FieldValues = Record<string, Date>;
type FieldContext = AnyObject;

type TestLabelledDateFieldProps = {
  form: UseFormReturn<FieldValues, FieldContext>;
};

const TestLabelledDateField = ({
  form,
}: TestLabelledDateFieldProps): ReactElement => {
  return (
    <FormProvider {...form}>
      <LabelledField
        name="birthday"
        type="date"
        label="Birthday"
        format="DD.MM.YYYY"
      />
    </FormProvider>
  );
};

class ResponseError extends Error {
  override name: 'ResponseError' = 'ResponseError' as const;

  constructor(
    public response: Response,
    msg?: string,
  ) {
    super(msg);
  }
}

const uniqueCheck = jest.fn();

function TestLabelledField(
  labelledFieldProps: Partial<LabelledFieldProps<AnyObject>>,
) {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <LabelledField
        name={testName}
        type="text"
        unique={uniqueCheck}
        label={testLabel}
        {...labelledFieldProps}
      />
    </FormProvider>
  );
}

describe('LabelledField', function () {
  const uniqueValidationErrorText = 'Must be unique.';
  const text = 'chuck_norris_research';

  beforeEach(() => uniqueCheck.mockClear());

  describe('without default value', function () {
    beforeEach(() => {
      render(<TestLabelledField />);
    });

    it.each`
      responseStatusCode      | isUniqueError
      ${StatusCodes.CONFLICT} | ${true}
      ${StatusCodes.OK}       | ${false}
    `(
      'should show unique validation error text ($isUniqueError) if response status code from backend is $responseStatusCode',
      async ({ responseStatusCode, isUniqueError }) => {
        if (isUniqueError) {
          uniqueCheck.mockImplementation(() => {
            throw new ResponseError(
              new Response(null, { status: StatusCodes.CONFLICT }),
            );
          });
        } else {
          uniqueCheck.mockResolvedValue(
            new Response(null, { status: responseStatusCode }),
          );
        }

        const input = await screen.findByRole('textbox');
        fireEvent.change(input, { target: { value: text } });
        fireEvent.blur(input); // triggers validation

        if (isUniqueError) {
          await screen.findByText(uniqueValidationErrorText);
        } else {
          expect(
            screen.queryByText(uniqueValidationErrorText),
          ).not.toBeInTheDocument();
        }

        expect(uniqueCheck).toHaveBeenCalledWith({ [testName]: text });
      },
    );
  });

  describe('with default value', function () {
    const defaultValue = 'chuck';

    beforeEach(() => {
      render(
        <TestLabelledField initialValues={{ [testName]: defaultValue }} />,
      );
    });

    it('should not show unique validation error text if the current value is equal to the initial value', async () => {
      const uniqueValidationErrorText = 'Must be unique.';

      uniqueCheck.mockImplementation(() => {
        throw new ResponseError(
          new Response(null, { status: StatusCodes.CONFLICT }),
        );
      });

      const input = await screen.findByRole('textbox');
      fireEvent.change(input, { target: { value: text } });
      fireEvent.blur(input); // triggers validation

      const uniqueValidationError = await screen.findByText(
        uniqueValidationErrorText,
      );

      fireEvent.change(input, { target: { value: defaultValue } });
      fireEvent.blur(input); // triggers validation

      await waitForElementToBeRemoved(uniqueValidationError);

      expect(uniqueCheck).toHaveBeenCalledWith({ [testName]: text });
    });
  });

  describe('with date type', function () {
    let form: UseFormReturn<FieldValues, FieldContext>;
    let spy: jest.SpyInstance;

    beforeEach(() => {
      spy = mockConsoleError();
      form = renderHook(() => useEnhancedForm<FieldValues, FieldContext>())
        .result.current;
    });

    afterEach(() => {
      spy.mockRestore();
    });

    it('`toISOString` should be UTC', async () => {
      render(<TestLabelledDateField form={form} />);
      // Enter the date picker text field
      const datePicker = await screen.findByRole('textbox');
      fireEvent.click(datePicker);
      const openCalendar = screen.getByRole('button');
      fireEvent.click(openCalendar);
      // Ensure calendar is open
      const previousMonth = screen.getByRole('button', {
        name: 'Previous month',
      });
      expect(previousMonth).toBeInTheDocument();
      const dayInputButton = screen.getByText('28');
      fireEvent.click(dayInputButton);
      // Ensure day is selected
      expect(form.getValues()['birthday'].toISOString()).toMatch(
        /T00:00:00.000Z$/,
      );
    });
  });
});
