import React, { ReactElement, useCallback, useState } from 'react';

import {
  ColumnDef,
  createColumnHelper,
  FilterFnOption,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import MaterialUiTable from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import { Typography } from '@mui/material';

import { AdditionalActionButtonFactory, IdType } from '../../../types';
import {
  ListBase,
  useDeleteDialog,
  UseListProps,
  useLoadItems,
  UseLoadItemsProps,
} from '../list';
import clsx from 'clsx';
import { GlobalClass } from '../../widgets';
import isFunction from 'lodash/isFunction';
import { TablePaginationActions } from './TablePaginationActions';
import { TableToolbar } from './TableToolbar';
import { useTableActionsCell } from './TableHooks';

export type EnhancedTableProps<T extends IdType> = Pick<
  UseListProps<T>,
  'getDeleteConfirmationText'
> &
  UseLoadItemsProps<T> & {
    addButtonLabel?: string;
    additionalActionButtons?: Array<AdditionalActionButtonFactory<T>>;
    canAdd?: boolean;
    canDelete?: boolean | ((item?: T) => boolean);
    canEdit?: boolean | ((item?: T) => boolean);
    canExport?: boolean;
    canFilter?: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    columns: ColumnDef<T, any>[];
    emptyMessage?: string;
    form?: React.ReactNode;
    globalFilterFn?: FilterFnOption<T>;
    globalFilterHelperText?: string;
    inline?: boolean;
    isExporting?: boolean;
    isFetching: boolean;
    isSubmitting: boolean;
    onAdd?: () => void;
    onDelete?: (itemId: number) => void;
    onEdit?: (item: T) => void;
    onExport?: () => void;
    onRowClick?: (item: T) => void;
    pagination?: boolean;
    title?: string;
  };

export const actionsColumnHeaderLabel = 'Actions';

export function EnhancedTable<T extends IdType>({
  columns,
  itemList,
  title,
  onRowClick,
  canAdd,
  canEdit,
  canDelete,
  canExport,
  onAdd,
  onEdit,
  onDelete,
  onExport,
  addButtonLabel,
  loadItems,
  isFetching,
  isSubmitting,
  isExporting = false,
  inline,
  emptyMessage,
  form,
  canFilter = true,
  globalFilterFn,
  pagination = true,
  getDeleteConfirmationText,
  additionalActionButtons,
  globalFilterHelperText,
}: EnhancedTableProps<T>): ReactElement {
  const {
    handleDeleteOpen,
    handleDeleteClose,
    deleteOpen,
    deleteConfirmationText,
  } = useDeleteDialog({
    deleteItem: onDelete,
  });
  const { hasAdditionalActionButtons, TableActionsCell } = useTableActionsCell({
    addButtonLabel,
    onEdit,
    handleDeleteOpen,
    getDeleteConfirmationText,
    itemList,
    additionalActionButtons,
  });
  const canEditAtLeastOne = isFunction(canEdit)
    ? itemList.some((row) => canEdit(row))
    : canEdit;
  const canDeleteAtLeastOne = isFunction(canDelete)
    ? itemList.some((row) => canDelete(row))
    : canDelete;
  const showActions = !!(
    canEditAtLeastOne ||
    canDeleteAtLeastOne ||
    hasAdditionalActionButtons
  );
  const actionsColumnId = 'actions';

  useLoadItems({ itemList, loadItems });

  const [globalFilter, setGlobalFilter] = useState('');
  const [sorting, setSorting] = React.useState<SortingState>([]);

  const columnHelper = createColumnHelper<T>();
  const actionColumn = showActions
    ? [
        columnHelper.display({
          id: 'actions',
          header: actionsColumnHeaderLabel,
          cell: TableActionsCell,
        }),
      ]
    : [];

  const table = useReactTable<T>({
    autoResetPageIndex: false,
    columns: [...columns, ...actionColumn],
    data: itemList,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    globalFilterFn: globalFilterFn ?? 'auto',
    onGlobalFilterChange: setGlobalFilter,
    onSortingChange: setSorting,
    state: {
      sorting,
      globalFilter,
    },
  });

  const {
    getAllColumns,
    getHeaderGroups,
    setPageIndex,
    setPageSize,
    getRowModel,
    getState,
  } = table;

  const handleChangePage = useCallback(
    (_event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) =>
      setPageIndex(newPage),
    [setPageIndex],
  );

  const handleChangeRowsPerPage = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setPageSize(Number(event.target.value));
    },
    [setPageSize],
  );

  const handleSetGlobalFilter = useCallback(
    (filterValue?: string) => {
      /**
       * Prevents no results being visible in case of the current page being greater than
       * the amount of results after filtering.
       *
       * For example: current page is `41-50`, but there are only 3 results after filtering.
       * The results after filtering are only visible on the first page, which means no results
       * are visible unless the user navigates to the first page manually.
       */
      if (getState().pagination.pageIndex !== 0) {
        setPageIndex(0);
      }
      setGlobalFilter(filterValue ?? '');
    },
    [getState, setPageIndex, setGlobalFilter],
  );

  const unsortableColumnId = actionsColumnId;
  const isEmpty = !itemList || !itemList.length;

  const globalFilterProps =
    canFilter && !isEmpty
      ? {
          setGlobalFilter: handleSetGlobalFilter,
          globalFilter: globalFilter,
          globalFilterHelperText: globalFilterHelperText,
        }
      : undefined;

  return (
    <ListBase
      deleteOpen={deleteOpen}
      handleDeleteClose={handleDeleteClose}
      isFetching={isFetching}
      itemList={itemList}
      canAdd={canAdd}
      deleteConfirmationText={deleteConfirmationText}
      inline={inline}
      form={form}
      isSubmitting={isSubmitting}
    >
      <TableContainer>
        <TableToolbar
          globalFilterProps={globalFilterProps}
          title={title}
          canAdd={canAdd}
          onAdd={onAdd}
          canExport={canExport}
          onExport={onExport}
          addButtonLabel={addButtonLabel}
          inline={inline}
          isExporting={isExporting}
        />
        {!isEmpty && (
          <MaterialUiTable>
            <TableHead>
              {getHeaderGroups().map((headerGroup) => (
                // key is included in `getHeaderGroupProps`
                <TableRow
                  sx={{
                    verticalAlign: 'text-top',
                  }}
                  key={headerGroup.id}
                >
                  {headerGroup.headers.map((header) => (
                    // key is included in `getHeaderProps`
                    <TableCell
                      sx={{
                        fontWeight: 'bold',
                        ...(header.column.columnDef.enableResizing &&
                          header.getSize() && {
                            width: `${header.getSize()}%`,
                          }),
                      }}
                      key={header.id}
                      title={
                        header.id !== unsortableColumnId
                          ? `Sort By ${header.column.columnDef.header}`
                          : undefined
                      }
                      onClick={header.column.getToggleSortingHandler()}
                    >
                      {flexRender(
                        header.column.columnDef.header,
                        header.getContext(),
                      )}
                      {header.id !== unsortableColumnId ? (
                        <TableSortLabel
                          active={!!header.column.getIsSorted()}
                          // react-table has a unsorted state which is not treated here
                          direction={header.column.getIsSorted() || 'asc'}
                        />
                      ) : null}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody>
              {getRowModel().rows.map((row) => (
                <TableRow key={row.id}>
                  {row.getVisibleCells().map((cell) => {
                    const isColumnSelectable =
                      cell.column.id !== unsortableColumnId;
                    const dataProps = { 'data-testid': cell.id };
                    return (
                      <TableCell
                        key={cell.id}
                        onClick={(): void => {
                          if (isColumnSelectable && !!onRowClick) {
                            onRowClick(row.original);
                          }
                        }}
                        className={clsx({
                          [GlobalClass.selectable]:
                            isColumnSelectable && !!onRowClick,
                        })}
                        {...dataProps}
                      >
                        {flexRender(cell.column.columnDef.cell, {
                          ...cell.getContext(),
                          ...(cell.column.id === actionsColumnId
                            ? { canEdit, canDelete, additionalActionButtons }
                            : {}),
                        })}
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                {pagination && (
                  <TablePagination
                    sx={{ borderBottom: 'none' }}
                    rowsPerPageOptions={[
                      5,
                      10,
                      25,
                      { label: 'All', value: itemList.length },
                    ]}
                    colSpan={getAllColumns().length}
                    count={table.getFilteredRowModel().rows.length}
                    page={getState().pagination.pageIndex}
                    rowsPerPage={getState().pagination.pageSize}
                    SelectProps={{
                      inputProps: { 'aria-label': 'rows per page' },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                )}
                {!pagination && (
                  <TableCell sx={{ border: 'none' }}>&nbsp;</TableCell>
                )}
              </TableRow>
            </TableFooter>
          </MaterialUiTable>
        )}
        {isEmpty && !!emptyMessage && (
          <Typography
            variant="body2"
            component="pre"
            sx={{
              paddingLeft: 2,
              paddingTop: 2,
            }}
          >
            {emptyMessage}
          </Typography>
        )}
      </TableContainer>
    </ListBase>
  );
}
