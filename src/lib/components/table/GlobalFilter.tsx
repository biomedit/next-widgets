import React, { ReactElement } from 'react';

import { Box } from '@mui/material';
import HelpIcon from '@mui/icons-material/Help';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';

import { Tooltip } from '../../widgets';

export type GlobalFilterProps = {
  globalFilter: string;
  globalFilterHelperText?: string;
  setGlobalFilter: (filterValue?: string) => void;
};

export function GlobalFilter({
  globalFilter,
  setGlobalFilter,
  globalFilterHelperText,
}: GlobalFilterProps): ReactElement {
  // Global filter only works with pagination from the first page.
  // This may not be a problem for server side pagination when
  // only the current page is downloaded.

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        gap: 1,
        marginRight: '50px',
      }}
    >
      <SearchIcon />
      <InputBase
        value={globalFilter || ''}
        onChange={(e) => {
          setGlobalFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
        }}
        placeholder={`Search`}
        inputProps={{ 'aria-label': 'search' }}
      />
      {globalFilterHelperText && (
        <Tooltip title={globalFilterHelperText}>
          <HelpIcon aria-label={'search-tooltip'} />
        </Tooltip>
      )}
    </Box>
  );
}
