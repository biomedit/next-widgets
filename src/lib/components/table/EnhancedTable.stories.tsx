import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { createColumnHelper } from '@tanstack/react-table';
import { EnhancedTable } from './EnhancedTable';
import { FabButton } from '../../widgets';

type ListItem = {
  description: string;
  id: number;
  name: string;
};

const itemList: ListItem[] = [
  {
    id: 0,
    name: 'Item 1',
    description: 'This is item 1',
  },
  {
    id: 1,
    name: 'Item 2',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris pretium, elit in viverra porttitor, ' +
      'est mi faucibus diam, vitae lobortis sapien lorem a odio. Praesent suscipit commodo libero, id interdum ' +
      'sapien commodo ac. Vestibulum a turpis non enim fermentum fringilla. Morbi ex eros, pulvinar vel nulla id, ' +
      'ultrices feugiat odio. Sed ut lorem nisl. Donec eleifend vehicula fringilla. Phasellus fermentum a velit ' +
      'tincidunt vehicula. Nulla vehicula finibus imperdiet. Nunc sollicitudin rutrum risus. Duis condimentum ' +
      'nec turpis gravida volutpat. Suspendisse rutrum lacus nisi, et tincidunt dolor consequat vitae.',
  },
];

const columnHelper = createColumnHelper<ListItem>();

const isFirstItem = (item?: ListItem) => item?.name === itemList[0].name;

export default {
  title: 'Components/Table/EnhancedTable',
  component: EnhancedTable,
} as Meta<typeof EnhancedTable>;

const Template: StoryFn<typeof EnhancedTable<ListItem>> = (args) => {
  const columns = [
    columnHelper.accessor('name', {
      header: 'Name',
    }),
    columnHelper.accessor('description', {
      header: 'Description',
      sortingFn: 'alphanumeric',
    }),
  ];

  return (
    <EnhancedTable<ListItem>
      {...args}
      columns={columns}
      itemList={itemList}
      isFetching={false}
      isSubmitting={false}
      addButtonLabel={'Custom Item'}
      inline
    />
  );
};

const Template2: StoryFn<typeof EnhancedTable<ListItem>> = (args) => {
  const columns = [
    columnHelper.accessor('name', {
      header: 'Name',
      size: 40,
      enableResizing: true,
    }),
    columnHelper.accessor('description', {
      header: 'Description',
      sortingFn: 'alphanumeric',
    }),
  ];

  return (
    <EnhancedTable<ListItem>
      {...args}
      columns={columns}
      itemList={itemList}
      isFetching={false}
      isSubmitting={false}
      addButtonLabel={'Custom Item'}
      inline
    />
  );
};

export const Default = Template.bind({});
Default.args = {};

export const WithTitleInline = Template.bind({});
WithTitleInline.args = { title: 'Custom Item', inline: true };

export const CanAdd = Template.bind({});
CanAdd.args = { canAdd: true };

export const CanAddWithTitle = Template.bind({});
CanAddWithTitle.args = {
  canAdd: true,
  canExport: true,
  title: 'Custom Item',
  inline: true,
};

export const CanExport = Template.bind({});
CanExport.args = { canExport: true };

export const CanEdit = Template.bind({});
CanEdit.args = { canEdit: true };

export const CanEditFunction = Template.bind({});
CanEditFunction.args = { canEdit: isFirstItem };

export const CanDelete = Template.bind({});
CanDelete.args = { canDelete: true };

export const CanDeleteFunction = Template.bind({});
CanDeleteFunction.args = { canDelete: isFirstItem };

export const WithDeleteConfirmationText = Template.bind({});
WithDeleteConfirmationText.args = {
  ...CanDelete.args,
  getDeleteConfirmationText: (item: ListItem) =>
    'Type this to delete ' + item.name,
};

export const CanEditAndDelete = Template.bind({});
CanEditAndDelete.args = { ...CanEdit.args, ...CanDelete.args };

export const WithAdditionalActionButtons = Template.bind({});
WithAdditionalActionButtons.args = {
  additionalActionButtons: [
    (item?: ListItem) => (
      <FabButton
        key={item?.id}
        icon="search"
        size="small"
        // eslint-disable-next-line no-console
        onClick={() => console.log(item)}
      />
    ),
  ],
};

export const CanEditAndDeleteWithAdditionalActionButtons = Template.bind({});
CanEditAndDeleteWithAdditionalActionButtons.args = {
  ...WithAdditionalActionButtons.args,
  ...CanEditAndDelete.args,
};

export const WithGlobalFilterHelperText = Template.bind({});
WithGlobalFilterHelperText.args = {
  globalFilterHelperText: "Hi! I'm a tooltip",
};

export const WithNameColumnWidthFortyPercent = Template2.bind({});
