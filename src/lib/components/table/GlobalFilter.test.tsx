import * as React from 'react';

import { render, screen } from '@testing-library/react';
import { GlobalFilter } from './GlobalFilter';

describe('GlobalFilter', function () {
  it.each`
    globalFilterHelperText  | expected
    ${undefined}            | ${false}
    ${''}                   | ${false}
    ${"Hi! I'm a tooltip!"} | ${true}
  `(
    'Should show ($expected) the tooltip if globalFilterHelperText is $globalFilterHelperText',
    async ({ globalFilterHelperText, expected }) => {
      render(
        <GlobalFilter
          globalFilter={''}
          setGlobalFilter={() => undefined}
          globalFilterHelperText={globalFilterHelperText}
        />,
      );

      if (expected) {
        await screen.findByLabelText('search-tooltip');
      } else {
        expect(
          screen.queryByLabelText('search-tooltip'),
        ).not.toBeInTheDocument();
      }
    },
  );
});
