import * as React from 'react';
import { ReactElement } from 'react';

import MuiLink from '@mui/material/Link';
import { styled } from '@mui/material/styles';

const StyledLink = styled(MuiLink)(({ theme }) => ({
  zIndex: theme.zIndex.tooltip + 1,
  position: 'fixed',
  top: theme.spacing(-10),
  left: theme.spacing(2),
  padding: theme.spacing(1),
  background: theme.palette.background.paper,
  outlineColor: theme.palette.secondary.main,
  transition: theme.transitions.create('top', {
    easing: theme.transitions.easing.easeIn,
    duration: theme.transitions.duration.leavingScreen,
  }),
  '&:focus': {
    top: theme.spacing(2),
    transition: theme.transitions.create('top', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  '@media print': {
    display: 'none',
  },
}));

export type SkipLinkProps = {
  href?: string;
  text?: string;
};

/**
 * Adds a skip link / bypass block to a page to improve accessibility according
 * to (WCAG 2.1, 2.4.1, Level A).
 *
 * By pressing the tab key after navigating to the page, a link appears in the
 * top left corner with the specified {@link text}.
 * After pressing enter, the focus will jump to the specified {@link href}.
 *
 * To integrate this into a website, it is recommended to wrap the part which
 * contains the main content in a {@code <main id="main-content"></main>} block.
 * Then, add this component somewhere early in the DOM, preferably as the first
 * element inside the {@link ThemeProvider}.
 * @param {string} [text=Skip to content] - The text to be shown when pressing
 *                                          the tab key.
 * @param {string} [href=#main-content] - Where the link should lead to when
 *                                        pressing enter, usually prefixed with
 *                                        {@code #} to jump to an {@code id}.
 */
export const SkipLink = ({ text, href }: SkipLinkProps): ReactElement => (
  <StyledLink color="primary" href={href ?? '#main-content'}>
    {text ?? 'Skip to content'}
  </StyledLink>
);
