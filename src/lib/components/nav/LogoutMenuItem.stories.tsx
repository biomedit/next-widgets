import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { LogoutMenuItem } from './LogoutMenuItem';
import { UserMenu } from './UserMenu';

export default {
  title: 'Components/Nav/LogoutMenuItem',
  component: LogoutMenuItem,
} as Meta<typeof LogoutMenuItem>;

const Template: StoryFn<typeof LogoutMenuItem> = () => {
  return (
    <UserMenu>
      <LogoutMenuItem logoutUrl={'javascript:void(0)'} />
    </UserMenu>
  );
};

export const Default = Template.bind({});
Default.args = {};
