import React from 'react';

import { HomeIcon, MessageIcon, ProjectIcon } from '../../widgets';
import { Meta, StoryFn } from '@storybook/react';
import { Nav } from './Nav';
import { NavItem } from './NavItem';

const homeLink = '/';
const projectLink = '/projects';

const menuItems = [
  {
    title: 'Home',
    link: homeLink,
    icon: <HomeIcon />,
  },
  {
    title: 'Projects',
    link: projectLink,
    icon: <ProjectIcon />,
  },
  {
    title: 'Contact',
    link: '/contact',
    icon: <MessageIcon />,
  },
];

export default {
  title: 'Components/Nav/Nav',
  component: Nav,
} as Meta<typeof Nav>;

const Template: StoryFn<Record<'selectedLink', string> & typeof Nav> = ({
  selectedLink,
  ...args
}) => {
  return (
    <Nav {...args}>
      {menuItems.map((menuItem) => (
        <NavItem
          selected={menuItem.link == selectedLink}
          href={menuItem.link ?? '#'}
          key={'MenuItem-' + menuItem.title}
          primary={menuItem.title}
          icon={menuItem.icon}
        />
      ))}
    </Nav>
  );
};

export const NoSelection = Template.bind({});
NoSelection.args = {};

export const HomeSelected = Template.bind({});
HomeSelected.args = { selectedLink: homeLink };

export const ProjectsSelected = Template.bind({});
ProjectsSelected.args = { selectedLink: projectLink };

export const WithCustomColors = Template.bind({});
WithCustomColors.args = {
  ...ProjectsSelected.args,
  backgroundColor: 'secondary.main',
};
