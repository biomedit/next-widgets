import React, { ReactElement } from 'react';

import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

import { getCsrfToken } from '../../api';
import { LogoutIcon } from '../../widgets/icons';

type LogoutMenuItemProps = {
  logoutUrl: string;
};

export const LogoutMenuItem = ({
  logoutUrl,
}: LogoutMenuItemProps): ReactElement => {
  return (
    <form action={logoutUrl} method="post">
      <input type="hidden" name="csrfmiddlewaretoken" value={getCsrfToken()} />
      <ListItemButton type={'submit'} href={logoutUrl}>
        <ListItemIcon>
          <LogoutIcon />
        </ListItemIcon>
        <ListItemText primary="Sign out" />
      </ListItemButton>
    </form>
  );
};
