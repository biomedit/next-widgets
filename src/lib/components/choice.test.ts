import { choicesByValue, isChoice, useEnumChoices } from './choice';
import { renderHook } from '@testing-library/react';

enum TestEnum {
  PRODUCTION = 'PRODUCTION',
  TEST = 'TEST',
}

describe('choice', function () {
  describe('isChoice', function () {
    it('should return true when it is a Choice instance', () => {
      const { result } = renderHook(() => useEnumChoices(TestEnum));
      expect(isChoice()).toEqual(false);
      expect(isChoice(undefined)).toEqual(false);
      expect(
        isChoice({
          key: 'test',
          value: 'test',
          label: 'test',
        }),
      ).toEqual(true);
      expect(isChoice(result.current[0])).toEqual(true);
    });
  });
  describe('choicesByValue', function () {
    it('should return the choices keyed by their value', () => {
      const { result } = renderHook(() => useEnumChoices(TestEnum));
      const purposeChoices = result.current;
      const choices = choicesByValue(purposeChoices);
      expect(Object.keys(choices)).toHaveLength(2);
      const choice = choices['TEST'];
      expect(choice.key).toBe('TEST');
      expect(choice.label).toBe('TEST');
    });
  });
});
