import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { CopyToClipboardPasswordField } from './CopyToClipboardPasswordField';

export default {
  title: 'Components/CopyToClipboardPasswordField',
  component: CopyToClipboardPasswordField,
} as Meta<typeof CopyToClipboardPasswordField>;

const Template: StoryFn<typeof CopyToClipboardPasswordField> = (args) => (
  <CopyToClipboardPasswordField {...args} value="Kerckhoffs's principle" />
);

export const Default = Template.bind({});
