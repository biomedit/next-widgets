import React, { ReactElement, useCallback } from 'react';

import ListItemIcon, { ListItemIconClassKey } from '@mui/material/ListItemIcon';
import ListItemText, { ListItemTextClassKey } from '@mui/material/ListItemText';
import { ClassNameMap } from '@mui/material';
import ListItemButton from '@mui/material/ListItemButton';

import { Action } from 'redux';
import Link from 'next/link';
import { useDispatch } from 'react-redux';

type ListItemBaseProps = {
  icon?: React.ReactNode;
  iconClasses?: Partial<ClassNameMap<ListItemIconClassKey>>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  listItemProps?: any;
  primary?: React.ReactNode;
  secondary?: React.ReactNode;
  selected?: boolean;
  textClasses?: Partial<ClassNameMap<ListItemTextClassKey>>; // FIXME 2020-07-12, François Martin
};

const ListItemBase = React.forwardRef<HTMLButtonElement, ListItemBaseProps>(
  (
    {
      icon,
      primary,
      secondary,
      textClasses,
      iconClasses,
      listItemProps,
      ...other
    },
    ref,
  ): ReactElement => (
    <ListItemButton ref={ref} {...listItemProps} {...other}>
      {icon && (
        <ListItemIcon
          sx={{ color: 'rgb(117, 117, 117)' }}
          classes={iconClasses}
        >
          {icon}
        </ListItemIcon>
      )}
      <ListItemText
        primary={primary}
        secondary={secondary}
        sx={{
          '& .MuiListItemText-primary': {
            fontSize: 18,
            textDecoration: 'none',
            textDecorationStyle: 'unset',
          },
        }}
        classes={textClasses}
      />
    </ListItemButton>
  ),
);
ListItemBase.displayName = 'ListItemBase';

export type ListItemLinkProps = ListItemBaseProps & {
  href: string;
};

export const ListItemLink = ({
  ...listItemBaseProps
}: ListItemLinkProps): ReactElement => {
  listItemBaseProps.listItemProps = {
    ...listItemBaseProps.listItemProps,
    component: Link,
  };
  return <ListItemBase {...listItemBaseProps} />;
};

export type ListItemActionProps = ListItemBaseProps & {
  action: Action;
};

export const ListItemAction = ({
  action,
  ...listItemBaseProps
}: ListItemActionProps): ReactElement => {
  const dispatch = useDispatch();
  const onClick = useCallback(() => dispatch(action), [action, dispatch]);

  listItemBaseProps.listItemProps = {
    ...listItemBaseProps.listItemProps,
    onClick,
  };

  return <ListItemBase {...listItemBaseProps} />;
};
