import React, { FunctionComponent } from 'react';

import { AdditionalActionButtonFactory, IdType } from '../../../types';
import { ListModel, useList, UseListProps } from './ListHooks';
import { $Values } from 'utility-types';
import { renderHook } from '@testing-library/react';

type CanEdit = $Values<Pick<UseListProps<IdType>, 'canEdit'>>;

describe('ListHooks', function () {
  describe('useList', function () {
    const openForm = jest.fn();
    const isId0: CanEdit = (item?: IdType) => item?.id === 0;
    const isId1: CanEdit = (item?: IdType) => item?.id === 1;
    const isId2: CanEdit = (item?: IdType) => item?.id === 2;
    const additionalEmpty: AdditionalActionButtonFactory<IdType> = () => null;
    const additionalAlways: AdditionalActionButtonFactory<IdType> = () => (
      <br key="br-always" />
    );
    const additionalId0: AdditionalActionButtonFactory<IdType> = (item) =>
      isId0(item) ? <br key="br-0" /> : null;
    const additionalId1: AdditionalActionButtonFactory<IdType> = (item) =>
      isId1(item) ? <br key="br-1" /> : null;
    const additionalId2: AdditionalActionButtonFactory<IdType> = (item) =>
      isId2(item) ? <br key="br-2" /> : null;

    let itemList: IdType[];
    let model: ListModel<IdType>;

    beforeEach(() => {
      openForm.mockReset();
      itemList = [{ id: 0 }, { id: 1 }];
      model = {
        fields: [
          {
            caption: 'ID',
            key: 'id',
            render: jest.fn() as FunctionComponent<IdType>,
          },
        ],
      };
    });

    it.each`
      openForm     | canDelete | canEdit  | additionalActionButtons                | id0HasActionButtons | id1HasActionButtons
      ${undefined} | ${false}  | ${false} | ${[]}                                  | ${false}            | ${false}
      ${undefined} | ${true}   | ${false} | ${[]}                                  | ${false}            | ${false}
      ${undefined} | ${false}  | ${true}  | ${[]}                                  | ${false}            | ${false}
      ${undefined} | ${true}   | ${true}  | ${[]}                                  | ${false}            | ${false}
      ${undefined} | ${false}  | ${isId0} | ${[]}                                  | ${false}            | ${false}
      ${undefined} | ${false}  | ${isId1} | ${[]}                                  | ${false}            | ${false}
      ${undefined} | ${false}  | ${isId2} | ${[]}                                  | ${false}            | ${false}
      ${openForm}  | ${false}  | ${false} | ${[]}                                  | ${false}            | ${false}
      ${openForm}  | ${true}   | ${false} | ${[]}                                  | ${true}             | ${true}
      ${openForm}  | ${false}  | ${true}  | ${[]}                                  | ${true}             | ${true}
      ${openForm}  | ${true}   | ${true}  | ${[]}                                  | ${true}             | ${true}
      ${openForm}  | ${false}  | ${isId0} | ${[]}                                  | ${true}             | ${false}
      ${openForm}  | ${false}  | ${isId1} | ${[]}                                  | ${false}            | ${true}
      ${openForm}  | ${false}  | ${isId2} | ${[]}                                  | ${false}            | ${false}
      ${undefined} | ${false}  | ${false} | ${[additionalEmpty]}                   | ${false}            | ${false}
      ${undefined} | ${true}   | ${false} | ${[additionalEmpty]}                   | ${false}            | ${false}
      ${undefined} | ${true}   | ${true}  | ${[additionalEmpty]}                   | ${false}            | ${false}
      ${undefined} | ${false}  | ${false} | ${[additionalAlways]}                  | ${true}             | ${true}
      ${undefined} | ${true}   | ${false} | ${[additionalAlways]}                  | ${true}             | ${true}
      ${undefined} | ${false}  | ${true}  | ${[additionalAlways]}                  | ${true}             | ${true}
      ${undefined} | ${true}   | ${true}  | ${[additionalAlways]}                  | ${true}             | ${true}
      ${undefined} | ${true}   | ${true}  | ${[additionalId0]}                     | ${true}             | ${false}
      ${undefined} | ${true}   | ${true}  | ${[additionalId1]}                     | ${false}            | ${true}
      ${undefined} | ${true}   | ${true}  | ${[additionalId2]}                     | ${false}            | ${false}
      ${undefined} | ${true}   | ${true}  | ${[additionalId0, additionalId1]}      | ${true}             | ${true}
      ${openForm}  | ${false}  | ${false} | ${[additionalEmpty]}                   | ${false}            | ${false}
      ${openForm}  | ${true}   | ${false} | ${[additionalEmpty]}                   | ${true}             | ${true}
      ${openForm}  | ${true}   | ${true}  | ${[additionalEmpty]}                   | ${true}             | ${true}
      ${openForm}  | ${false}  | ${false} | ${[additionalAlways]}                  | ${true}             | ${true}
      ${openForm}  | ${true}   | ${false} | ${[additionalAlways]}                  | ${true}             | ${true}
      ${openForm}  | ${false}  | ${true}  | ${[additionalAlways]}                  | ${true}             | ${true}
      ${openForm}  | ${true}   | ${true}  | ${[additionalAlways]}                  | ${true}             | ${true}
      ${openForm}  | ${false}  | ${false} | ${[additionalEmpty, additionalAlways]} | ${true}             | ${true}
      ${openForm}  | ${false}  | ${false} | ${[additionalId0]}                     | ${true}             | ${false}
      ${openForm}  | ${false}  | ${false} | ${[additionalId1]}                     | ${false}            | ${true}
      ${openForm}  | ${false}  | ${false} | ${[additionalId2]}                     | ${false}            | ${false}
      ${openForm}  | ${false}  | ${false} | ${[additionalId0, additionalId1]}      | ${true}             | ${true}
    `(
      'should have action buttons on item with ID 0 ($id0HasActionButtons) and ID 1 ($id1HasActionButtons), when canDelete is $canDelete and canEdit is $canEdit, with openForm: $openForm and amount of additional buttons: $additionalActionButtons.length',
      ({
        openForm,
        canDelete,
        canEdit,
        additionalActionButtons,
        id0HasActionButtons,
        id1HasActionButtons,
      }) => {
        const { result } = renderHook(() =>
          useList<IdType>({
            openForm: openForm,
            canDelete: canDelete,
            canEdit: canEdit,
            itemList: itemList,
            model: model,
            additionalActionButtons,
          }),
        );

        expect(result.current.hasActionButtons).toBe(
          id0HasActionButtons || id1HasActionButtons,
        );

        const items = result.current.items;
        expect(items).toHaveLength(2);

        const id0 = items[0];
        const id1 = items[1];

        expect(id0.id).toBe(itemList[0].id);
        expect(id1.id).toBe(itemList[1].id);

        expect(id0.actionButtons !== null).toBe(id0HasActionButtons);
        expect(id1.actionButtons !== null).toBe(id1HasActionButtons);
      },
    );

    it.each`
      getCaption                                                                     | expectedCaptions
      ${undefined}                                                                   | ${['', '']}
      ${(item: IdType) => `Item ID: ${item.id}`}                                     | ${['Item ID: 0', 'Item ID: 1']}
      ${(item: IdType, index: number) => `Item ID: ${item.id}, Index: ${index + 1}`} | ${['Item ID: 0, Index: 1', 'Item ID: 1, Index: 2']}
    `(
      'should return captions $expectedCaptions when getCaption is $getCaption',
      ({ getCaption, expectedCaptions }) => {
        model.getCaption = getCaption;

        const { result } = renderHook(() =>
          useList<IdType>({
            itemList: itemList,
            model: model,
          }),
        );

        const captions = result.current.items.map((item) => item.caption);
        expect(captions).toEqual(expectedCaptions);
      },
    );
  });
});
