import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { DeleteDialog } from './DeleteDialog';

export default {
  title: 'Components/List/DeleteDialog',
  component: DeleteDialog,
} as Meta<typeof DeleteDialog>;

const Template: StoryFn<typeof DeleteDialog> = (args) => (
  <DeleteDialog
    {...args}
    onDelete={(isConfirmed) =>
      // eslint-disable-next-line no-console
      console.log('onDelete, isConfirmed:', isConfirmed)
    }
    isSubmitting={false}
  />
);

export const Default = Template.bind({});
Default.args = { open: true };

export const WithItemName = Template.bind({});
WithItemName.args = { ...Default.args, itemName: 'Repository' };

export const WithConfirmationText = Template.bind({});
WithConfirmationText.args = {
  ...WithItemName.args,
  confirmationText: 'next-widgets',
};
