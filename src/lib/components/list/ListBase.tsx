import React, { ReactElement } from 'react';

import { CircularProgress, Typography } from '@mui/material';

import { AddIconButton, ButtonBar } from '../../widgets';
import { DeleteDialog } from './DeleteDialog';

export type ListBaseProps<T> = {
  canAdd?: boolean;
  children?: React.ReactNode | React.ReactNode[];
  deleteConfirmationText?: string;
  deleteOpen?: boolean;
  emptyMessage?: string;
  form?: React.ReactNode | null;
  // if true, the list is not displayed on a full page, but used inline inside of a page
  handleDeleteClose?: (isConfirmed: boolean) => void;
  inline?: boolean;
  isFetching: boolean;
  isSubmitting: boolean;
  itemList: T[];
  itemName?: string;
  openForm?: () => void;
};

export function ListBase<T>({
  canAdd,
  emptyMessage,
  itemList,
  itemName,
  deleteConfirmationText,
  handleDeleteClose,
  inline,
  children,
  deleteOpen,
  isFetching,
  isSubmitting,
  form: Form,
  openForm,
}: ListBaseProps<T>): ReactElement {
  // To prevent, that onClick gets called with unpredicted arguments propagating to openForm:
  const openAddForm = openForm && (() => openForm());
  const addButton = canAdd && openAddForm && (
    <AddIconButton onClick={openAddForm} itemName={itemName} />
  );
  const isEmpty = !itemList || !itemList.length;

  if (isFetching) {
    return <CircularProgress />;
  }
  return (
    <>
      {isEmpty && emptyMessage ? (
        <Typography variant="body2" component="pre">
          {emptyMessage}
        </Typography>
      ) : (
        children
      )}
      {!inline && <ButtonBar>{addButton}</ButtonBar>}
      <DeleteDialog
        open={!!deleteOpen}
        itemName={itemName}
        confirmationText={deleteConfirmationText}
        isSubmitting={isSubmitting}
        onDelete={(isConfirmed) =>
          handleDeleteClose && handleDeleteClose(isConfirmed)
        }
      />
      {Form}
    </>
  );
}
