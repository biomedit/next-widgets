import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { ConfirmDialog } from './ConfirmDialog';

export default {
  title: 'Components/List/ConfirmationDialog',
  component: ConfirmDialog,
} as Meta<typeof ConfirmDialog>;

const Template: StoryFn<typeof ConfirmDialog> = (args) => (
  <ConfirmDialog
    {...args}
    title="Confirmation Title"
    text="Text communicating something the user needs to confirm"
    onConfirm={(isConfirmed) =>
      // eslint-disable-next-line no-console
      console.log('onConfirm, isConfirmed:', isConfirmed)
    }
    isSubmitting={false}
  />
);

export const Open = Template.bind({});
Open.args = { open: true };

export const Closed = Template.bind({});
Closed.args = { open: false };

export const WithConfirmationText = Template.bind({});
WithConfirmationText.args = { ...Open.args, confirmationText: 'next-widgets' };
