import React, { ReactElement } from 'react';

import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MaterialUiList from '@mui/material/List';

export type ListProps = {
  icon?: React.ReactNode;
  items: string[];
};

export function List(props: ListProps): ReactElement {
  const { items, icon } = props;
  return (
    <MaterialUiList>
      {items.map((item) => (
        <ListItem key={item}>
          {icon && <ListItemIcon>{icon}</ListItemIcon>}
          <ListItemText primary={item} />
        </ListItem>
      ))}
    </MaterialUiList>
  );
}
