import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { Chip } from './Chip';
import { HomeIcon } from './icons';

export default {
  title: 'Widgets/Chip',
  component: Chip,
} as Meta<typeof Chip>;

const Template: StoryFn<typeof Chip> = (args) => (
  <Chip icon={<HomeIcon />} label={'Home'} {...args} />
);

export const Default = Template.bind({});
Default.args = {};

export const Primary = Template.bind({});
Primary.args = { color: 'primary' };

export const Secondary = Template.bind({});
Secondary.args = { color: 'secondary' };

export const Outlined = Template.bind({});
Outlined.args = { variant: 'outlined' };

export const Small = Template.bind({});
Small.args = { size: 'small' };
