import React, { ReactElement } from 'react';

import { Link } from '@mui/material';
import { LinkProps } from '@mui/material/Link';
import { Required } from 'utility-types';

type SecureLinkProps = Required<Omit<LinkProps, 'rel' | 'target'>, 'href'>;

export const SafeLink = ({
  children,
  ...linkProps
}: SecureLinkProps): ReactElement => {
  return (
    <Link {...linkProps} target={'_blank'} rel={'noopener noreferrer'}>
      {children}
    </Link>
  );
};
