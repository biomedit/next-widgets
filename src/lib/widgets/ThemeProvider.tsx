import React, { ComponentPropsWithoutRef, ReactElement } from 'react';

import { alpha, CssBaseline } from '@mui/material';
import {
  createTheme,
  ThemeProvider as MuiThemeProvider,
  StyledEngineProvider,
} from '@mui/material/styles';
import { Optional } from 'utility-types';

export enum GlobalClass {
  selectable = 'selectable',
}

export const baseTheme = createTheme({
  palette: {
    primary: {
      main: '#010101',
      light: '#646363',
    },
    secondary: {
      main: '#e30613',
      light: '#ee7559',
    },
    background: { default: 'white' },
  },
  components: {
    MuiButtonBase: {
      styleOverrides: {
        root: ({ theme }) => ({
          '&:focus-visible': {
            backgroundColor: alpha(theme.palette.secondary.main, 0.5),
          },
          '&.Mui-selected': {
            '&.Mui-focusVisible': {
              background: alpha(theme.palette.secondary.main, 0.5),
            },
          },
        }),
      },
    },
    MuiListItem: {
      styleOverrides: {
        root: ({ theme }) => ({
          '&:focus-visible': {
            backgroundColor: alpha(theme.palette.secondary.main, 0.5),
          },
          '&.Mui-selected': {
            '&.Mui-focusVisible': {
              background: alpha(theme.palette.secondary.main, 0.5),
            },
          },
        }),
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          fontSize: '0.825rem',
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        subtitle1: {
          fontWeight: 'bold',
          lineHeight: 'inherit',
        },
      },
    },
    MuiCssBaseline: {
      styleOverrides: {
        ['.' + GlobalClass.selectable]: {
          cursor: 'pointer',
        },
      },
    },
  },
});

export const ThemeProvider = ({
  children,
  theme,
}: Optional<
  ComponentPropsWithoutRef<typeof MuiThemeProvider>,
  'theme'
>): ReactElement => (
  <StyledEngineProvider injectFirst>
    <MuiThemeProvider theme={theme ?? baseTheme}>
      <CssBaseline />
      {children}
    </MuiThemeProvider>
  </StyledEngineProvider>
);
