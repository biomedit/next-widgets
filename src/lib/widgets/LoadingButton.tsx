import React, { ReactElement } from 'react';

import { Button, ButtonProps, CircularProgress } from '@mui/material';
import { styled } from '@mui/material/styles';

const Wrapper = styled('div')(() => ({
  position: 'relative',
  display: 'inline-block',
}));

export type LoadingButtonProps = ButtonProps & { loading: boolean };

export const LoadingButton = ({
  loading,
  children,
  ...buttonProps
}: LoadingButtonProps): ReactElement => {
  return (
    <Wrapper>
      <Button {...buttonProps} disabled={loading || buttonProps.disabled}>
        {children}
      </Button>
      {loading && (
        <CircularProgress
          size={24}
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: -1.5,
            marginLeft: -1.5,
          }}
        />
      )}
    </Wrapper>
  );
};
