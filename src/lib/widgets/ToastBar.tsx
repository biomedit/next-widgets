import React, { ReactElement, useCallback } from 'react';

import { Alert, AlertProps } from '@mui/material';
import Snackbar, { SnackbarProps } from '@mui/material/Snackbar';
import { SnackbarCloseReason } from '@mui/material/Snackbar/Snackbar';

import { clearToast, ToastSeverity, ToastState } from '../reducers';
import { useDispatch, useSelector } from 'react-redux';
import { DefaultRootState } from '../../types';
import { mailto } from '../utils';

export type InternalErrorContentProps = ToastBarProps & {
  correlationId: string;
};

export const InternalErrorContent = ({
  correlationId,
  subjectPrefix,
  contactEmail,
}: InternalErrorContentProps): ReactElement => {
  return (
    <span>
      An unexpected error occurred. Please{' '}
      <a
        href={mailto(
          contactEmail ?? '',
          `${subjectPrefix}Unexpected Error`,
          `Correlation ID: ${correlationId}

Reproduction steps:
(please explain step by step what you did before the error happened)

Expected behavior:
(please explain what you would expect to happen if everything is working correctly)

Actual behavior:
(please explain what happened instead of what you expected)`,
        )}
      >
        contact us
      </a>{' '}
      about this issue and refer to this ID: {correlationId}
    </span>
  );
};

type ToastBarSnackBarProps = Pick<AlertProps, 'children' | 'onClose'> &
  Pick<SnackbarProps, 'onClose' | 'open'> &
  Pick<ToastState, 'severity'>;

export const ToastBarSnackBar = ({
  severity,
  open,
  onClose,
  children,
}: ToastBarSnackBarProps) => {
  return (
    <Snackbar
      open={open}
      onClose={onClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      autoHideDuration={severity === ToastSeverity.ERROR ? null : 6000}
    >
      <div>
        {severity && (
          <Alert
            onClose={severity === ToastSeverity.ERROR ? onClose : undefined}
            elevation={6}
            variant="filled"
            severity={severity}
            sx={{
              '& a': {
                color: 'white',
              },
            }}
          >
            {children}
          </Alert>
        )}
      </div>
    </Snackbar>
  );
};

type ToastBarProps = {
  contactEmail: string;
  subjectPrefix: string;
};

export function ToastBar({
  subjectPrefix,
  contactEmail,
}: ToastBarProps): ReactElement {
  const dispatch = useDispatch();

  const { message, severity, correlationId } = useSelector<
    DefaultRootState,
    ToastState
  >((state) => state.toast);

  const handleSnackBarClose = useCallback(
    (_event?: Event | React.SyntheticEvent, reason?: SnackbarCloseReason) => {
      if (reason === 'clickaway' && severity === ToastSeverity.ERROR) {
        return;
      }
      dispatch(clearToast());
    },
    [severity, dispatch],
  );

  return (
    <ToastBarSnackBar
      open={message !== undefined || correlationId !== undefined}
      onClose={handleSnackBarClose}
      severity={severity}
    >
      {correlationId ? (
        <InternalErrorContent
          correlationId={correlationId}
          subjectPrefix={subjectPrefix}
          contactEmail={contactEmail}
        />
      ) : (
        <span>{message}</span>
      )}
    </ToastBarSnackBar>
  );
}
