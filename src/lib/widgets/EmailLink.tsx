import React, { ReactElement, ReactNode } from 'react';

import { mailto } from '../utils';

export type EmailLinkProps = {
  body?: string;
  children?: ReactNode;
  email: string;
  subject?: string;
};

export const EmailLink = ({
  email,
  subject,
  body,
  children,
}: EmailLinkProps): ReactElement => {
  return <a href={mailto(email, subject, body)}>{children ?? email}</a>;
};
