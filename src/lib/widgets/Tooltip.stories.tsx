import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { Tooltip } from './Tooltip';

export default {
  title: 'Widgets/Tooltip',
  component: Tooltip,
} as Meta<typeof Tooltip>;

const Template: StoryFn<typeof Tooltip> = (args) => {
  return <Tooltip title="This is a tooltip!">{args.children}</Tooltip>;
};

export const Text = Template.bind({});
Text.args = {
  children: <span>Test</span>,
};
