import React, { ReactElement, ReactNode } from 'react';

import MuiTimeline from '@mui/lab/Timeline';
import MuiTimelineConnector from '@mui/lab/TimelineConnector';
import MuiTimelineContent from '@mui/lab/TimelineContent';
import MuiTimelineDot from '@mui/lab/TimelineDot';
import MuiTimelineItem from '@mui/lab/TimelineItem';
import MuiTimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import MuiTimelineSeparator from '@mui/lab/TimelineSeparator';

import { formatDate, formatTimestamp } from '../utils';
import { Typography, TypographyProps } from '@mui/material';
import { $Keys } from 'utility-types';
import { TestId } from '../testId';
import { timelineIcons } from './icons';

export type TimelineElement = {
  date?: Date;
  icon: $Keys<typeof timelineIcons>;
  message?: ReactNode;
  messageProps?: Omit<TypographyProps, 'ref'>;
  title?: ReactNode;
  titleProps?: Omit<TypographyProps, 'ref'>;
};

export type TimelineProps = React.ComponentPropsWithoutRef<
  typeof MuiTimeline
> & {
  elements: TimelineElement[];
};

export function Timeline({
  elements,
  ...timelineProps
}: TimelineProps): ReactElement {
  return (
    <MuiTimeline {...timelineProps}>
      {elements.map((element: TimelineElement, index: number) => (
        <MuiTimelineItem
          key={`timeline-item-${formatTimestamp(element.date, true)}-${
            element.title
          }-${element.message}`}
        >
          <MuiTimelineOppositeContent
            sx={{
              paddingTop: 1,
              // prevent wasting space on the left
              maxWidth: 'max-content',
              minWidth: 'max-content',
            }}
          ></MuiTimelineOppositeContent>

          <MuiTimelineSeparator sx={{ paddingBottom: 1 }}>
            <MuiTimelineDot
              variant="outlined"
              sx={{
                marginTop: 0,
                marginBottom: 1,
              }}
              data-testid={TestId.TIMELINE_DOT_PREFIX + element.icon}
            >
              {timelineIcons[element.icon]}
            </MuiTimelineDot>
            {index < elements.length - 1 && (
              <MuiTimelineConnector data-testid={TestId.TIMELINE_CONNECTOR} />
            )}
          </MuiTimelineSeparator>
          <MuiTimelineContent sx={{ paddingTop: 0.5, paddingBottom: 3 }}>
            {element.date && (
              <Typography variant="body2" color="textSecondary">
                {formatDate(element.date)}
              </Typography>
            )}
            {element.title && (
              <Typography variant="h6" component="h1" {...element.titleProps}>
                {element.title}
              </Typography>
            )}
            {element.message && (
              <Typography
                sx={element.title ? undefined : { paddingTop: 0.5 }}
                {...element.messageProps}
              >
                {element.message}
              </Typography>
            )}
          </MuiTimelineContent>
        </MuiTimelineItem>
      ))}
    </MuiTimeline>
  );
}
