import { default as MuiToggleButtonGroup } from '@mui/material/ToggleButtonGroup';
import { styled } from '@mui/material/styles';

export const ToggleButtonGroup = styled(MuiToggleButtonGroup)(() => ({
  marginRight: '2rem',
}));
