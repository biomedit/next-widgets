import React, { ReactElement, ReactNode } from 'react';

import { Box, BoxProps } from '@mui/material';

type FixedChildrenHeightProps = BoxProps & {
  childHeight?: number | string;
  children: ReactNode[];
};

/**
 * Wraps any non-falsey child into a div and sets the height.
 *
 * This is especially useful for components with helper text: it avoids the
 * jumping when the error text becomes visible.
 */
export const FixedChildrenHeight = React.memo(
  ({
    children,
    childHeight = '5rem',
    ...boxProps
  }: FixedChildrenHeightProps): ReactElement => (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        '&> *': {
          minHeight: childHeight,
          marginRight: 'auto',
        },
      }}
      {...boxProps}
    >
      {children}
    </Box>
  ),
);

FixedChildrenHeight.displayName = 'FixedChildrenHeight';
