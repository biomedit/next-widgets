import React, { useState } from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { ToggleButton } from './Buttons';
import { ToggleButtonGroup } from './ToggleButtonGroup';

enum ToggleState {
  OFF = 0,
  ON = 1,
}

export default {
  title: 'Widgets/ToggleButtonGroup',
  component: ToggleButtonGroup,
} as Meta<typeof ToggleButtonGroup>;

const Template: StoryFn<typeof ToggleButtonGroup> = ({ value, ...args }) => {
  const [toggleState, setToggleState] = useState<ToggleState | undefined>(
    value,
  );
  return (
    <ToggleButtonGroup
      {...args}
      value={toggleState}
      exclusive
      onChange={(event, value) => {
        // eslint-disable-next-line no-console
        console.log('Event:', event, 'Value:', value);
        if (value !== null) {
          setToggleState(value);
        }
      }}
    >
      {[ToggleState.ON, ToggleState.OFF].map((enumItem) => {
        return (
          <ToggleButton key={enumItem} value={enumItem}>
            {ToggleState[enumItem]}
          </ToggleButton>
        );
      })}
    </ToggleButtonGroup>
  );
};

export const NoValue = Template.bind({});
NoValue.args = {};

export const On = Template.bind({});
On.args = { value: ToggleState.ON };

export const Off = Template.bind({});
Off.args = { value: ToggleState.OFF };
