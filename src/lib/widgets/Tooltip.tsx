import React, { ReactElement } from 'react';

import { Tooltip as MaterialUiTooltip, TooltipProps } from '@mui/material';

export const Tooltip = (props: TooltipProps): ReactElement => (
  <MaterialUiTooltip {...props} arrow sx={{ maxWidth: 310 }} />
);

type TooltipTextProps = Omit<TooltipProps, 'children' | 'title'> & {
  // allow tooltip to be empty, which will just display the string by itself
  text: string;
  title?: string;
};

export const TooltipText = ({
  text,
  ...tooltipProps
}: TooltipTextProps): ReactElement => {
  const textNode = <span>{text}</span>;
  if (tooltipProps.title && tooltipProps.title.length) {
    return (
      <Tooltip title {...tooltipProps}>
        {textNode}
      </Tooltip>
    );
  }
  return textNode;
};
