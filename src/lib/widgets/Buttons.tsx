import React, { CSSProperties, ReactElement } from 'react';

import {
  AddIcon,
  ArchiveIcon,
  CreateIcon,
  DeleteIcon,
  DownloadIcon,
  MarkEmailReadIcon,
  MarkEmailUnreadIcon,
  SearchIcon,
  UnarchiveIcon,
} from './icons';
import { blue, brown, green, purple, red, teal } from './Colors';
import Fab, { FabProps } from '@mui/material/Fab';
import { LinkProps } from 'next/link';
import { Tooltip } from './Tooltip';

export { default as Button } from '@mui/material/Button';
export { default as ToggleButton } from '@mui/material/ToggleButton';

type FabStyles = Record<string, FabStyle>;

type FabStyle = {
  buttonStyle: CSSProperties;
  icon: React.ElementType;
  iconStyle?: CSSProperties;
};

const styles: FabStyles = {
  add: { icon: AddIcon, buttonStyle: { backgroundColor: 'primary.main' } },
  search: { icon: SearchIcon, buttonStyle: { backgroundColor: teal[500] } },
  edit: { icon: CreateIcon, buttonStyle: { backgroundColor: green[400] } },
  del: {
    icon: DeleteIcon,
    buttonStyle: { backgroundColor: red[500] },
  },
  download: {
    icon: DownloadIcon,
    buttonStyle: { backgroundColor: blue[500] },
  },
  archive: {
    icon: ArchiveIcon,
    buttonStyle: { backgroundColor: brown[500] },
  },
  unarchive: {
    icon: UnarchiveIcon,
    buttonStyle: { backgroundColor: purple[500] },
  },
  markEmailRead: {
    icon: MarkEmailReadIcon,
    buttonStyle: { backgroundColor: brown[500] },
  },
  markEmailUnread: {
    icon: MarkEmailUnreadIcon,
    buttonStyle: { backgroundColor: purple[500] },
  },
};

type FabBaseProps = ActionProps & {
  icon: keyof typeof styles;
};

type ActionProps = Partial<FabProps> &
  Partial<LinkProps> & {
    tooltip?: string;
  };

function FabBase(displayName: string) {
  return Object.assign(
    ({ icon, ...fabProps }: FabBaseProps): ReactElement => {
      const fabStyle = styles[icon];
      const Icon = fabStyle.icon;
      let FabContent = <Icon sx={fabStyle.iconStyle ?? { fill: 'white' }} />;
      if (fabProps.tooltip) {
        FabContent = <Tooltip title={fabProps.tooltip}>{FabContent}</Tooltip>;
      }
      return (
        <Fab
          sx={{
            marginRight: 'inherit',
            aspectRatio: '1',
            // Needed because otherwise minWidth is 0 and overrides aspectRatio
            minWidth: 'auto',
            ...fabStyle.buttonStyle,
            boxShadow: 'none',
            '&:hover': { background: 'LightGray' },
          }}
          {...fabProps}
        >
          {FabContent}
        </Fab>
      );
    },
    { displayName },
  );
}

export const FabButton = FabBase('FabButton');

export const addIconButtonLabelPrefix = 'Add ';
export const editIconButtonLabelPrefix = 'Edit ';
export const deleteIconButtonLabelPrefix = 'Delete ';

export const itemNameGeneric = 'item';

export type IconButtonProps = ActionProps & {
  itemName?: string;
};

export const AddIconButton = ({
  itemName,
  ...actionProps
}: IconButtonProps): ReactElement => {
  const label = addIconButtonLabelPrefix + (itemName ?? itemNameGeneric);
  return (
    <FabButton
      size="small"
      icon="add"
      aria-label={label}
      title={label}
      {...actionProps}
    />
  );
};

export const EditIconButton = ({
  itemName,
  ...actionProps
}: IconButtonProps): ReactElement => {
  const label = editIconButtonLabelPrefix + (itemName ?? itemNameGeneric);
  return (
    <FabButton
      size="small"
      icon="edit"
      aria-label={label}
      title={label}
      {...actionProps}
    />
  );
};

export const DeleteIconButton = ({
  itemName,
  ...actionProps
}: IconButtonProps): ReactElement => {
  const label = deleteIconButtonLabelPrefix + (itemName ?? itemNameGeneric);
  return (
    <FabButton
      size="small"
      icon="del"
      aria-label={label}
      title={label}
      {...actionProps}
    />
  );
};
