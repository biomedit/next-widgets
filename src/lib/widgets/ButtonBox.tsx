import { styled } from '@mui/material/styles';

export const ButtonBox = styled('div')(() => ({ marginRight: 5 }));
