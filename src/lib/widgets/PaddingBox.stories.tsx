import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { PaddingBox } from './PaddingBox';

export default {
  title: 'Widgets/PaddingBox',
  component: PaddingBox,
} as Meta<typeof PaddingBox>;

const Template: StoryFn<typeof PaddingBox> = (args) => {
  return <PaddingBox>{args.children}</PaddingBox>;
};

export const Text = Template.bind({});
Text.args = {
  children: <p>Test</p>,
};
