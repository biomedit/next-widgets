import React, { ReactElement, ReactNode } from 'react';

import MaterialUiChip, {
  ChipProps as MaterialUiChipProps,
} from '@mui/material/Chip';
import Avatar from '@mui/material/Avatar';

import { FaceIcon } from './icons';
import { Tooltip } from './Tooltip';

type ChipProps = MaterialUiChipProps & {
  children?: null;
  icon?: ReactNode;
};

export const Chip = React.forwardRef<HTMLDivElement, ChipProps>(
  ({ icon, ...other }, ref) => (
    <MaterialUiChip
      style={{ marginBottom: 4 }}
      avatar={<Avatar>{icon}</Avatar>}
      ref={ref}
      {...other}
    />
  ),
);
Chip.displayName = 'Chip';

type ChipWithTooltipProps = ChipProps & {
  tooltip?: string | null;
};

export const ChipWithTooltip = ({
  tooltip,
  ...other
}: ChipWithTooltipProps): ReactElement => {
  if (tooltip) {
    return <Tooltip title={tooltip}>{<Chip {...other} />}</Tooltip>;
  } else {
    return <Chip {...other} />;
  }
};

type UserChipProps = ChipWithTooltipProps & {
  user: string | undefined;
};

export const UserChip = ({ user, ...props }: UserChipProps): ReactElement => (
  <ChipWithTooltip icon={<FaceIcon />} label={user} {...props} />
);
