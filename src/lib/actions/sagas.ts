import { Action, UnknownAction } from 'redux';
import { ActionType, AnyActionType, ApiFunction } from './actionTypes';
import {
  call,
  ForkEffect,
  put,
  takeLatest,
  takeLeading,
} from 'redux-saga/effects';
import { Environment, redirectExternal } from '../api';
import {
  getCorrelationId,
  isError,
  isResponseError,
  serializeFetch,
  serializeResponse,
} from '../utils';
import { ObjectOf, ResponseError } from '../../types';
import { setToast, ToastSeverity } from '../reducers';
import { logger } from '../logger';
import { StatusCodes } from 'http-status-codes';

const log = logger('Sagas');

interface OnSuccessAction extends Action {
  onSuccess?: UnknownAction;
}

function takeApi(
  actionType: AnyActionType,
  apiObject: unknown,
  loginUrl: string,
  latest: boolean,
  handledErrors?: StatusCodes[],
): ForkEffect<never> {
  const boundApi = actionType.apiFunction.bind(apiObject) as ApiFunction<
    OnSuccessAction,
    unknown
  >;

  function* saga(action: OnSuccessAction): Generator {
    try {
      const result = yield call(boundApi, action);
      const resultAction = {
        type: actionType.success,
        requestArgs: action,
        response: result,
      };
      yield put(resultAction);
      if (action.onSuccess !== undefined) {
        yield put(action.onSuccess);
      }
    } catch (error) {
      // MUST be an `Error` object
      if (isError(error)) {
        const errorAction = { type: actionType.failure, error: error };
        if (isResponseError(error)) {
          const { response } = error;
          if (!handledErrors?.includes(response.status)) {
            yield put(errorAction);
            yield handleError(error, actionType, loginUrl);
          } else {
            const serializedError = yield call(serializeResponse, response);
            yield put({
              type: actionType.failure,
              error: serializedError,
            });
          }
        } else {
          yield put(errorAction);
        }
      } else {
        // Propagate the error to the caller as we don't know what to do with it
        throw error;
      }
    }
  }

  if (latest) {
    return takeLatest(actionType.request, saga);
  } else {
    return takeLeading(actionType.request, saga);
  }
}

type TakeApiFunction = (
  actionType: AnyActionType,
  handledErrors?: StatusCodes[],
) => ForkEffect<never>;

export type TakeApi = {
  latest: TakeApiFunction;
  leading: TakeApiFunction;
};

/**
 * Creates an object which gives access to the equivalent of {@link takeLatest}
 * and {@link takeLeading} method equivalents to perform API calls.
 *
 * @param apiObject API object on which the functions passed in as `apiFunction`
 *                  will be called.
 * @param loginUrl to redirect to if the user is not logged in when making a
 *                 request.
 */
export const takeApiFactory = (
  apiObject: unknown,
  loginUrl: string,
): TakeApi => ({
  latest: (actionType: AnyActionType, handledErrors?: StatusCodes[]) =>
    takeApi(actionType, apiObject, loginUrl, true, handledErrors),
  leading: (actionType: AnyActionType, handledErrors?: StatusCodes[]) =>
    takeApi(actionType, apiObject, loginUrl, false, handledErrors),
});

function* handleError<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
>(
  responseError: ResponseError,
  actionType: ActionType<RequestParams, ResponseType>,
  loginUrl: string,
) {
  if (process.env.NODE_ENV === Environment.TEST) {
    // Don't swallow errors during testing
    throw responseError;
  }
  const { response } = responseError;
  const errorId = getCorrelationId(response);
  const errorLog = {
    action: actionType.failure,
    json: {},
    correlationId: errorId,
    message: 'Saga threw an error.',
  };
  const errorAction = setToast({
    severity: ToastSeverity.ERROR,
    correlationId: errorId,
  });
  if (isSessionExpired(response)) {
    errorAction.severity = ToastSeverity.WARNING;
    errorAction.message = 'Please wait until we log you back in...';
    yield put(errorAction);
    redirectExternal(loginUrl);
    // In case of `StatusCodes.CONFLICT` we NEVER log the error
  } else if (response.status !== StatusCodes.CONFLICT) {
    serializeFetch(response, (res) => {
      errorLog.json = res;
      log.error(errorLog);
    });
    yield put(errorAction);
  } else {
    // wrap into object in case `errorWithResponse` is not an object
    // to avoid deserialization errors
    errorLog.json = { responseError };
    log.error(errorLog);
    yield put(errorAction);
  }
}

function isSessionExpired(response: Response) {
  return response.status === StatusCodes.FORBIDDEN;
}

type DestinationAction = Action & {
  destination: string;
};

export function* redirect({ destination }: DestinationAction) {
  yield call(redirectExternal, destination);
}
