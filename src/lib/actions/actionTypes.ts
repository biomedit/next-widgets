import { Action, Dispatch } from 'redux';
import { ObjectOf } from '../../types';
import { useDispatch } from 'react-redux';

export type ApiFunction<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
> =
  | (() => ResponseType)
  | ((requestParams: RequestParams) => ResponseType)
  | ((requestParams: RequestParams) => void);

export type ActionType<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
  Name extends string = string,
> = {
  apiFunction: ApiFunction<RequestParams, ResponseType>;
  failure: `${Name}_FAILURE`;
  request: `${Name}_REQUEST`;
  success: `${Name}_SUCCESS`;
};

export type AnyActionType = ActionType<never, unknown>;

export function declareAction<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
  Name extends string = string,
>(
  name: Name,
  apiFunction: ApiFunction<RequestParams, ResponseType>,
): ActionType<RequestParams, Awaited<ResponseType>, typeof name> {
  return {
    request: `${name}_REQUEST` as const,
    success: `${name}_SUCCESS` as const,
    failure: `${name}_FAILURE` as const,
    apiFunction,
  };
}

export const requestAction = <
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
  OnSuccessAction extends Action = Action,
  Name extends string = string,
>(
  actionType: ActionType<RequestParams, ResponseType, Name>,
  requestParams: RequestParams = {} as RequestParams,
  onSuccess?: OnSuccessAction,
): Action<typeof actionType.request> & RequestParams => ({
  type: actionType.request,
  ...requestParams,
  onSuccess,
});

export const useReduxDispatch = <T extends string>() =>
  useDispatch<Dispatch<Action<T>>>();
