import 'react-redux';
import { type reducers } from '../lib/reducers/reducers';

/**
 * Uses TypeScript's module augmentation to re-define the type of {@code DefaultRootState}.
 *
 * This allows writing code like this:
 * ```typescript
 * useSelector(state => state.user)
 * ```
 * instead of this:
 * ```typescript
 * useSelector<State>(state => state.user)
 * ```
 *
 * @see https://github.com/DefinitelyTyped/DefinitelyTyped/pull/41031
 */
declare module 'react-redux' {
  export interface DefaultRootState extends ReturnType<typeof reducers> {} // eslint-disable-line @typescript-eslint/no-empty-object-type
}
