import { type reducers } from '../lib/reducers/reducers';

/**
 * This interface can be augmented by users to add default types for the root state when
 * using `react-redux`.
 * Use module augmentation to append your own type definition in a your_custom_type.d.ts file.
 * https://www.typescriptlang.org/docs/handbook/declaration-merging.html#module-augmentation
 *
 * ### Example
 * Add a new `.d.ts` file with the following content:
 * ```ts
 * import 'next-widgets';
 * import { YourRootState } from './store';
 *
 * declare module 'next-widgets' {
 *   // eslint-disable-next-line @typescript-eslint/no-empty-interface
 *   export interface DefaultRootState extends YourRootState {}
 * }
 * ```
 */
export type DefaultRootState = ReturnType<typeof reducers>;
