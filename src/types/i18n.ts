/**
 * Reflects the filenames in `public/locales/en`.
 * Make sure to add an entry here for every new file
 * and change the value when changing the filenames.
 */
import { GetStaticPropsContext, GetStaticPropsResult } from 'next';
import { ParsedUrlQuery } from 'querystring';
import { Required } from 'utility-types';

export type FrontendGetStaticProps<
  P extends { [key: string]: unknown } = { [key: string]: unknown },
  Q extends ParsedUrlQuery = ParsedUrlQuery,
> = (
  context: Required<GetStaticPropsContext<Q>, 'locale'>,
) => Promise<GetStaticPropsResult<P>>;
