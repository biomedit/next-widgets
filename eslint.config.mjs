import jsEslint from '@eslint/js';
import jsonEslint from 'eslint-plugin-jsonc';
import prettier from 'eslint-config-prettier';
import tsEslint from 'typescript-eslint';

import jestPlugin from 'eslint-plugin-jest';
import prettierPlugin from 'eslint-plugin-prettier/recommended';
import reactHooksPlugin from 'eslint-plugin-react-hooks';
import reactPlugin from 'eslint-plugin-react';
import sortKeysPlugin from 'eslint-plugin-typescript-sort-keys';
import storybookPlugin from 'eslint-plugin-storybook';

// ESlint settings and options for all files.
const tsConfig = tsEslint.config({
  settings: {
    react: {
      version: 'detect',
    },
  },
  files: ['**/*.json', '**/*.js', '**/*.jsx', '**/*.ts', '**/*.tsx'],
  ignores: ['coverage/', 'dist/', 'build/', 'esm/', 'lib/', 'storybook-static'],
  extends: [
    jsEslint.configs.recommended,
    ...tsEslint.configs.strict,
    prettier,
    jestPlugin.configs['flat/recommended'],
    prettierPlugin,
  ],
  plugins: {
    react: reactPlugin,
    'react-hooks': reactHooksPlugin,
    storybook: storybookPlugin,
    typescriptSortKeys: sortKeysPlugin,
  },
  languageOptions: {
    parser: tsEslint.parser,
    parserOptions: {
      project: './tsconfig.eslint.json',
    },
  },
  rules: {
    semi: ['warn', 'always'],
    quotes: [
      'warn',
      'single',
      { avoidEscape: true, allowTemplateLiterals: true },
    ],
    'no-nested-ternary': 'error',
    'linebreak-style': ['error', 'unix'],
    'max-len': [
      'error',
      {
        comments: 100,
        code: 99999999, // handled by prettier
        ignorePattern: 'eslint-disable', // comments to disable eslint cannot be shortened
        ignoreUrls: true,
      },
    ],
    'no-cond-assign': ['error', 'always'],
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    'sort-imports': [
      'error',
      {
        ignoreCase: true,
        ignoreDeclarationSort: false,
        ignoreMemberSort: false,
        memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
        allowSeparatedGroups: true,
      },
    ],
    // Turn off some of the recommended jest rules.
    'jest/no-identical-title': 'off', // Too many false-positives when using `each`
    'jest/no-conditional-expect': 'off',
    'jest/expect-expect': 'off', // Doesn't take implicit assertions into account, like when using `@testing-library/react` matchers for example
    // Use default rules of react and react-hooks. Adding rules in this way is
    // a hack needed because the plugin does not support ESlint 9.x.
    ...reactPlugin.configs.recommended.rules,
    ...reactHooksPlugin.configs.recommended.rules,
    'react/prop-types': 'off',
    'react/no-array-index-key': 'error',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/sort-type-constituents': 'error',
    '@typescript-eslint/no-unused-vars': [
      'error',
      { caughtErrorsIgnorePattern: '^_' },
    ],
    '@typescript-eslint/no-deprecated': 'error',
    '@typescript-eslint/switch-exhaustiveness-check': 'error',
    'typescriptSortKeys/interface': 'error',
    'typescriptSortKeys/string-enum': 'error',
  },
});

// ESlint settings and options specific to JSON files.
const jsonConfig = tsEslint.config({
  files: ['**/*.json'],
  ...jsonEslint.configs['flat/recommended-with-jsonc'],
});

// Set ESlint config values.
export default tsEslint.config(...tsConfig, ...jsonConfig);
