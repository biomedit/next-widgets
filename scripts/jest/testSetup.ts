// Mock cookies
document.cookie = 'Mookie';

// Mock .env
process.env.NEXT_PUBLIC_UNIQUE_VALIDATION_DEBOUNCE = '750';
process.env.NEXT_PUBLIC_CONTACT_EMAIL = 'chuck@norris.private';
